package com.example.bettype.exoticbetType;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bettype.R;
import com.example.bettype.adapters.TopFlucAdapter;
import com.example.bettype.model.FlucModel;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DuetExoticFragmnet extends Fragment {
    @BindView(R.id.reycler)
    RecyclerView reycler;
    TopFlucAdapter adapter;
    List<FlucModel> list = new ArrayList<>();
    Context sContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_duet_exotic, container, false);
        sContext = getActivity();
        sContext = getActivity();
        ButterKnife.bind(this, view);
        list.add(new FlucModel(R.drawable.ic_jointoday));
        reycler();
        return view;
    }

    private void reycler() {
        reycler.setLayoutManager(new GridLayoutManager(sContext, 2));
        adapter = new TopFlucAdapter((getActivity()), list);
        reycler.setAdapter(adapter);
        reycler.setNestedScrollingEnabled(false);

        adapter.onItemClickListner(new TopFlucAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.urlLink(sContext, "https://www.unibet.com.au/promotions?mktid=1:81748421:911995-30888");
                }
                else {

                } }
        });
    }
}