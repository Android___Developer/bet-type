package com.example.bettype.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class BookiesAdapter extends FragmentStatePagerAdapter {
    int noOfTab;
    public BookiesAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.noOfTab=tabCount; }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                default:
                return null; } }

    @Override
    public int getCount() {
        return noOfTab;
    }
}
