package com.example.bettype.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bettype.R;
import com.example.bettype.utilies.Global;

import java.util.List;

public class MultibetAdapter extends RecyclerView.Adapter<MultibetAdapter.Holder> {
    Context sContext;
    List<String> list;
    itemClickInterface click;
    Spinner spin_dead, spin_runner, spin_paid;
    String[] heat = {"First", "Second", "Third",};
    String[] paid = {"One", "Two", "Three",};
    String[] runner = {"Two", "Three", "Four",};
    String[] outcome = {"Win", "Loss", "Placed", "Dead Heat", "Void"};
    int pos=-1;
    public MultibetAdapter(Context sContext, List<String> list) {
        this.sContext = sContext;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(sContext).inflate(R.layout.item_multibet, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.text_count.setText(list.get(i));
        holder.spineer();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        Spinner spinner;
        TextView text_count;
        LinearLayout lin_spinner_item;
        Spinner spin_dead, spin_runner, spin_paid;

        public Holder(@NonNull View itemView) {
            super(itemView);
            spinner = itemView.findViewById(R.id.spinner);
            text_count = itemView.findViewById(R.id.text_count);
            lin_spinner_item = itemView.findViewById(R.id.lin_spinner_item);
            spin_dead = itemView.findViewById(R.id.spin_dead);
            spin_runner = itemView.findViewById(R.id.spin_runner);
            spin_paid = itemView.findViewById(R.id.spin_paid);
            Global.spinner(sContext, R.layout.spinner_item, heat, spin_dead);
            Global.spinner(sContext, R.layout.spinner_item, paid, spin_paid);
            Global.spinner(sContext, R.layout.spinner_item, runner, spin_runner);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onItemClick(getAdapterPosition());
                }}); }

        private void spineer() {
            ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, outcome);
            spinner.setAdapter(heatAdapter);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String text = parent.getSelectedItem().toString();
                    if (text.equals("Dead Heat")) {
                        lin_spinner_item.setVisibility(View.VISIBLE);
                    } else {
                        lin_spinner_item.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        }
    }

    public interface itemClickInterface {
        void onItemClick(int pos);
    }

    public void onItemCLickListener(itemClickInterface click) {
        this.click = click;
    }

}




