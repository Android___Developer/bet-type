package com.example.bettype.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bettype.R;
import com.example.bettype.model.FlucModel;

import java.util.List;

import static android.support.v7.widget.RecyclerView.*;

public class TopFlucAdapter extends RecyclerView.Adapter<TopFlucAdapter.Holder> {
    Context sContext;
    List<FlucModel> list;
    OnItemClickInterface click;

    public TopFlucAdapter(Context sContext, List<FlucModel> list) {
        this.sContext = sContext;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(sContext).inflate(R.layout.fluc_items, viewGroup, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int postion) {
        FlucModel model = list.get(postion);
        Glide.with(sContext).load(list.get(postion).getIcon()).into(holder.img_Category);

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends ViewHolder {
        ImageView img_Category;
        public Holder(@NonNull View itemView) {
            super(itemView);
            img_Category = itemView.findViewById(R.id.img_Category);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickInterface {
        void onItemClick(int pos);
    }

    public void onItemClickListner(OnItemClickInterface click) {
        this.click = click;
    }

}
