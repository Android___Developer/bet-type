package com.example.bettype.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bettype.R;

import java.util.List;

public class ArbitageAdapter extends RecyclerView.Adapter<ArbitageAdapter.Holder> {
    Context sContext;
    List<String> list;

    public ArbitageAdapter(Context sContext, List<String> list) {
        this.sContext = sContext;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(sContext).inflate(R.layout.item_arbt,viewGroup,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.text_count.setText(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView text_count;
        public Holder(@NonNull View itemView) {
            super(itemView);
            text_count = itemView.findViewById(R.id.text_count);
        }
    }
}
