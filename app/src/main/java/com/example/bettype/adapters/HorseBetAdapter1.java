package com.example.bettype.adapters;

import android.content.AsyncQueryHandler;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bettype.R;
import com.example.bettype.model.Horse1Model;
import com.example.bettype.model.HorseModel;

import java.util.ArrayList;
import java.util.List;


public class HorseBetAdapter1 extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {
    Context context;
    List<Horse1Model> mainlist;
    private List<Horse1Model> displayedList;
    OnItemClickInterface clickInterface;

    String[] outcome = {"Win", "Dead Heat", "Loss", "Placed", "Void"};
    String[] heat = {"FIRST", "SECOND", "THIRD",};
    String[] paid = {"ONE", "TWO", "THREE",};
    String[] runner = {"TWO", "THREE", "FOUR",};

    public HorseBetAdapter1(Context context, List<Horse1Model> mainlist) {
        this.context = context;
        this.mainlist = mainlist;
        this.displayedList = mainlist;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        switch (viewType) {

            case Horse1Model.ITEM:
                View view = layoutInflater.inflate(R.layout.horsetype_betting, viewGroup, false);
                holder = new ItemsViewHolder(view);
                break;

            case Horse1Model.HEADER:
                View view1 = layoutInflater.inflate(R.layout.win_heading, viewGroup, false);
                holder = new HeadingViewHolder(view1);
                break;

            default:
                View def = layoutInflater.inflate(R.layout.horsetype_betting, viewGroup, false);
                holder = new ItemsViewHolder(def);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        switch (holder.getItemViewType()) {
            case Horse1Model.HEADER:
                HeadingViewHolder headingViewHolder = (HeadingViewHolder) holder;
                headingViewHolder.heading_Text.setText("Exotic bet type");
                headingViewHolder.heading_Text.setTextSize(14);
                headingViewHolder.heading_Text.setPadding(0, 6, 0, 6);
                break;

            case Horse1Model.ITEM:
                ItemsViewHolder itemsViewHolder = (ItemsViewHolder) holder;
                int colortype = mainlist.get(i).getColor_type();
                itemsViewHolder.textView.setText(mainlist.get(i).getName());
                Glide.with(context).load(mainlist.get(i).getImage()).into(itemsViewHolder.imageView);

                if (colortype == 1) {
                    itemsViewHolder.textView.setTextColor(Color.parseColor("#052a3f"));
                    itemsViewHolder.imageView.setBackground(context.getResources().getDrawable(R.drawable.bestofbest_shape));
                } else if (colortype == 2) {
                    itemsViewHolder.textView.setTextColor(Color.parseColor("#219b8d"));
                    itemsViewHolder.imageView.setBackground(context.getResources().getDrawable(R.drawable.topfluc_shape));

                } else if (colortype == 3) {
                    itemsViewHolder.textView.setTextColor(Color.parseColor("#16645b"));
                    itemsViewHolder.imageView.setBackground(context.getResources().getDrawable(R.drawable.horse_shape));

                } else {
                    itemsViewHolder.textView.setTextColor(Color.parseColor("#052a3f"));
                    itemsViewHolder.imageView.setBackground(context.getResources().getDrawable(R.drawable.bestofbest_shape));
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return displayedList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mainlist.get(position).getViewType();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.length() == 0) {
                    displayedList = mainlist;
                } else {
                     List<Horse1Model> filteredList = new ArrayList<>();
                    for (Horse1Model model : mainlist) {
                        if (model.getName().toLowerCase().contains(charSequenceString.toLowerCase()) | model.getName().toUpperCase().contains(charSequenceString.toUpperCase())) {
                            filteredList.add(model);
                        }
                        displayedList = filteredList;

                    }
                }

                    FilterResults results = new FilterResults();
                    results.count = displayedList.size();
                    results.values = displayedList;
                    return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

//                if (displayedList != null) {
//                    mainlist = (List<HorseModel>) results.values;
//                } else {
//                    displayedList = mainlist;
//                }

                displayedList = (List<Horse1Model>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    private class ItemsViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RelativeLayout relative;

        public ItemsViewHolder(View itemview) {
            super(itemview);
            imageView = itemView.findViewById(R.id.image);
            textView = itemView.findViewById(R.id.text);
            relative = itemView.findViewById(R.id.relative);
            itemview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickInterface.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    private class HeadingViewHolder extends RecyclerView.ViewHolder {
        TextView heading_Text;

        public HeadingViewHolder(View itemview) {
            super(itemview);
            heading_Text = itemview.findViewById(R.id.heading_Text);

        }
    }

    public interface OnItemClickInterface {
        void onItemClick(int pos);
    }

    public void ItemCLick(OnItemClickInterface click) {
        this.clickInterface = click;
    }
}
