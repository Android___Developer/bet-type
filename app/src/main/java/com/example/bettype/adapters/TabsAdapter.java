package com.example.bettype.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;
import android.view.View;

import com.example.bettype.R;
import com.example.bettype.activities.MainActivity;
import com.example.bettype.fragments.Costcalculator;
import com.example.bettype.fragments.Oddcalculator;

public class TabsAdapter extends FragmentStatePagerAdapter {

    int noOfTab;

    private  FragmentManager mFragmentManager;
    public TabsAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.mFragmentManager=fm;
        this.noOfTab=tabCount;

    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                Oddcalculator oddcalculator = new Oddcalculator();
                return oddcalculator;
            case 1:

                Costcalculator costcalculator = new Costcalculator();

                return costcalculator;


            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return noOfTab;
    }
}
