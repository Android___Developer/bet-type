package com.example.bettype.adapters;

import android.content.AsyncQueryHandler;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bettype.R;
import com.example.bettype.activities.MainActivity;
import com.example.bettype.activities.OtherCalculators;
import com.example.bettype.activities.SplashActivity;
import com.example.bettype.activities.TopFlucActivty;
import com.example.bettype.fragments.HorseBetting;
import com.example.bettype.fragments.TopFlucFragment;
import com.example.bettype.model.HorseModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HorsebetAdapter extends RecyclerView.Adapter<HorsebetAdapter.Viewholder> implements Filterable {
    Context context;
    List<HorseModel> mainlist;
    private List<HorseModel> displayedList;
    private OnItemClickInterface click;
    String[] outcome = {"Win", "Dead Heat", "Loss", "Placed", "Void"};
    String[] heat = {"FIRST", "SECOND", "THIRD",};
    String[] paid = {"ONE", "TWO", "THREE",};
    String[] runner = {"TWO", "THREE", "FOUR",};

    public HorsebetAdapter(FragmentActivity activity, List<HorseModel> list) {
        super();
        this.context = activity;
        this.mainlist = list;
        this.displayedList = mainlist;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.horsetype_betting, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int i) {
        int colortype = mainlist.get(i).getColor_type();
        viewholder.textView.setText(mainlist.get(i).getName());
        Glide.with(context).load(mainlist.get(i).getImage()).into(viewholder.imageView);

        if (colortype == 1) {
            viewholder.textView.setTextColor(Color.parseColor("#052a3f"));
            viewholder.imageView.setBackground(context.getResources().getDrawable(R.drawable.bestofbest_shape));
        } else if (colortype == 2) {
            viewholder.textView.setTextColor(Color.parseColor("#219b8d"));
            viewholder.imageView.setBackground(context.getResources().getDrawable(R.drawable.topfluc_shape));

        } else if (colortype == 3) {
            viewholder.textView.setTextColor(Color.parseColor("#16645b"));
            viewholder.imageView.setBackground(context.getResources().getDrawable(R.drawable.horse_shape));

        } else {
            viewholder.textView.setTextColor(Color.parseColor("#052a3f"));
            viewholder.imageView.setBackground(context.getResources().getDrawable(R.drawable.bestofbest_shape));
        }
    }

    @Override
    public int getItemCount() {
        return mainlist.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charSequenceString = constraint.toString();
                if (charSequenceString.length()==0)  {
                    displayedList = mainlist;
                } else {
                    List<HorseModel> filteredList = new ArrayList<>();
                    for (HorseModel model : mainlist) {
                        if (model.getName().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            displayedList.add(model);
                        }
                        displayedList = filteredList; }
                }
                FilterResults results = new FilterResults();
                results.count = displayedList.size();
                results.values = displayedList;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

//                if (displayedList != null) {
//                    mainlist = (List<HorseModel>) results.values;
//                } else {
//                    displayedList = mainlist;
//                }

                displayedList = (List<HorseModel>) results.values;
                mainlist = displayedList;
                notifyDataSetChanged();
            }
        };
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        RelativeLayout relative;

        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);
            textView = itemView.findViewById(R.id.text);
            relative = itemView.findViewById(R.id.relative);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onItemClick(getAdapterPosition());
                }
            });
        }
    }

    public void onItemClickListner(OnItemClickInterface click) {
        this.click = click;
    }

    public interface OnItemClickInterface {
        void onItemClick(int pos);
    }
}
