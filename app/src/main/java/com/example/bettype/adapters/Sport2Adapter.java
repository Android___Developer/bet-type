package com.example.bettype.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.bettype.R;
import com.example.bettype.model.HorseModel;

import java.util.List;

public class Sport2Adapter extends RecyclerView.Adapter<Sport2Adapter.Viewholder> {

    Context context;
    List<HorseModel> mainlist;
    public Sport2Adapter(FragmentActivity activity, List<HorseModel> list) {
        this.context=activity;
        this.mainlist=list;
    }
    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.horsetype_betting,viewGroup,false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int i) {
        int colortype=mainlist.get(i).getColor_type();
        if(colortype==1)
        {
            viewholder.textView.setTextColor(Color.parseColor("#052a3f"));
            viewholder.imageView.setBackgroundColor(Color.parseColor("#052a3f"));
        }
        else if(colortype==2)
        {
            viewholder.textView.setTextColor(Color.parseColor("#08DAC1"));
            viewholder.imageView.setBackgroundColor(Color.parseColor("#08DAC1"));

        }
        else if(colortype==3)
        {
            viewholder.textView.setTextColor(Color.parseColor("#16645b"));
            viewholder.imageView.setBackgroundColor(Color.parseColor("#16645b"));

        }
        else {
            viewholder.textView.setTextColor(Color.parseColor("#052a3f"));
            viewholder.imageView.setBackgroundColor(Color.parseColor("#052a3f"));

        }
        viewholder.textView.setText(mainlist.get(i).getName());
        Glide.with(context).load(mainlist.get(i).getImage()).into(viewholder.imageView);
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class Viewholder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textView;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.image);
            textView=itemView.findViewById(R.id.text);
        }
    }
}
