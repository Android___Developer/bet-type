package com.example.bettype.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bettype.R;

import java.util.ArrayList;
import java.util.List;

public class PlaceOneAdapter extends RecyclerView.Adapter<PlaceOneAdapter.Holder> {
    Context sCOntext;
    List<String> list=new ArrayList<>();

    public PlaceOneAdapter(Context sCOntext, List<String> list) {
        this.sCOntext = sCOntext;
        this.list = list;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view= LayoutInflater.from(sCOntext).inflate(R.layout.item_one,viewGroup,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
           holder.text_one.setText(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView text_one;
        public Holder(@NonNull View itemView) {
            super(itemView);
            text_one=itemView.findViewById(R.id.text_one);
        }
    }
}
