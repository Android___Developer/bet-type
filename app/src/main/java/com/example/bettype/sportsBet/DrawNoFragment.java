package com.example.bettype.sportsBet;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.bettype.R;
import com.example.bettype.adapters.TopFlucAdapter;
import com.example.bettype.model.FlucModel;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DrawNoFragment extends Fragment {

    @BindView(R.id.reycler)
    RecyclerView reycler;
    TopFlucAdapter adapter;
    List<FlucModel> list = new ArrayList<>();
    Context sContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drawno_bet, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        list.add(new FlucModel(R.drawable.ic_beteasyy));
        list.add(new FlucModel(R.drawable.ic_unitbett));
        list.add(new FlucModel(R.drawable.ic_ladbroke));
        list.add(new FlucModel(R.drawable.ic_sportsbett));
        list.add(new FlucModel(R.drawable.ic_neds));
        list.add(new FlucModel(R.drawable.ic_betthreesixfive));
        reycler();
        return view;
    }
    private void reycler() {
        reycler.setLayoutManager(new GridLayoutManager(sContext, 2));
        adapter = new TopFlucAdapter((getActivity()), list);
        reycler.setAdapter(adapter);
        reycler.setNestedScrollingEnabled(false);
        adapter.onItemClickListner(new TopFlucAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.urlLink(sContext, "https://beteasy.com.au/join-now?");
                } else if (pos == 1) {
                    Global.urlLink(sContext, "https://www.unibet.com.au/promotions?mktid=1:81748421:911995-30888");
                } else if (pos == 2) {
                    Global.urlLink(sContext, "http://www.ladbrokes.com.au/signup/?a=510766&d=FBR500&c=2");
                } else if (pos == 3) {
                    Global.urlLink(sContext, "https://bet-types.com.au/to/sportsbet/offer/");

                } else if (pos == 4) {
                    Global.urlLink(sContext, "https://bet-types.com.au/to/neds/offer/");
                } else if (pos == 5) {
                    Global.urlLink(sContext, "https://www.bet365.com/en/?forcelp=1&affiliate=365_381347");
                } else {

                } }
        });}
}