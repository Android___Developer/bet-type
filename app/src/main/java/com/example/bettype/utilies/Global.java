package com.example.bettype.utilies;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bettype.R;

import static android.support.v4.content.ContextCompat.startActivity;

public class Global {


    public static void changeDashFragment(Context context, Fragment fragment)
    { FragmentManager fm=((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.replace(R.id.main_container,fragment).addToBackStack("tag").commit(); }

    public static void betCalculator(Context context, Fragment fragment)
    {
        FragmentManager fm=((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.replace(R.id.betcal_container,fragment).commit(); }

    public static void bettype(Context context, Fragment fragment)
    { FragmentManager fm=((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.replace(R.id.bettypee_container,fragment).commit();
    }

    public static void singleFragment(Context context, Fragment fragment)
    {
        FragmentManager fm=((FragmentActivity)context).getSupportFragmentManager();
        FragmentTransaction ft=fm.beginTransaction();
        ft.replace(R.id.single_container,fragment).addToBackStack("tag").commit();
    }


    public static void spinner(Context context,int layout, String[] list,Spinner spinner) {
        ArrayAdapter  adapter = new ArrayAdapter(context,layout,R.id.text,list);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
             String item = parent.getItemAtPosition(position).toString(); }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
     public static void urlLink(Context context,String link)
     {
         String url = link;
         Intent i = new Intent(Intent.ACTION_VIEW);
         i.setData(Uri.parse(url));
         context.startActivity(i);
     }




}
