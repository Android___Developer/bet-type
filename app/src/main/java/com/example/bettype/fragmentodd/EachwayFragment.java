package com.example.bettype.fragmentodd;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.bettype.R;
import com.example.bettype.activities.MainActivity1;
import com.example.bettype.utilies.EdittextCheck;
import com.example.bettype.utilies.Helper;
import butterknife.ButterKnife;


public class EachwayFragment extends Fragment {
    Context sContext;
    Spinner spinner_outcome, spin_runner, spin_paid, spin_dead, spinner_deduction, spinner_format;
    LinearLayout lin_deadheat_item, lin_deduction;
    EditText edbetamount, edWinOdds, edPlaceOdd, edDeduction;
    TextView txttotalStake, txtReutrn, txtProfit;
    AlertDialog alertDialog;
    private String fractionResult = "";
    private int res;
    int denominator = 1;


    String[] heat = {"First", "Second", "Third",};
    String[] paid = {"One", "Two", "Three",};
    String[] runner = {"Two", "Three", "Four",};
    String[] outcome = {"Win", "Loss", "Placed", "Dead Heat", "Void"};
    String[] deduction = {"No", "Yes"};

    String[] format = {"Decimal", "Fractional", "American"};
    Double edbetAmountD, edWinOddD, edPaceoddD, edDeductionD;
    String text = "";
    String deadHeatPostion, deadHeatPlace, deadHeatRunner;
    String deductionPostion, formatPostion, outcomePosition;
    ArrayAdapter heatAdapter;
    private int numerator1, demonintor1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_eachway, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        Helper.clearPref(sContext);
        findview(view);
        spinnerDeadHeat();
        postionDeadHeat();
        spinnerPlaces();
        spinnerRunner();
        keyboradDoneListener();
        MainActivity1.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAlertDailog();
            }
        });
        return view;
    }

    private void keyboradDoneListener() {
        edbetamount.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edbetamount.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            Helper.hideKeyboard((Activity) sContext);
                            edbetAmountD = Double.parseDouble(s);
                            checkCondition();
                            checkAlertCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        edWinOdds.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edWinOdds.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            Helper.hideKeyboard((Activity) sContext);
                            edWinOddD = Double.parseDouble(s);
                            checkCondition();
                            checkAlertCondition(); }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        edPlaceOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edPlaceOdd.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            Helper.hideKeyboard((Activity) sContext);
                            edPaceoddD = Double.parseDouble(s);
                            checkCondition();
                            checkAlertCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        edDeduction.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edDeduction.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            Helper.hideKeyboard((Activity) sContext);
                            edDeductionD = Double.parseDouble(s);
                            checkCondition();
                            checkAlertCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true; }
                return false;
            }
        });
    }

    private void findview(View view) {
        spinner_outcome = view.findViewById(R.id.spinner_outcome);
        spin_runner = view.findViewById(R.id.spin_runner);
        spin_paid = view.findViewById(R.id.spin_paid);
        spin_runner = view.findViewById(R.id.spin_runner);
        spin_dead = view.findViewById(R.id.spin_dead);

        edbetamount = view.findViewById(R.id.edbetamount);
        edWinOdds = view.findViewById(R.id.edWinOdds);
        edPlaceOdd = view.findViewById(R.id.edPlaceOdd);
        txttotalStake = view.findViewById(R.id.txttotalStake);
        txtReutrn = view.findViewById(R.id.txtReutrn);
        txtProfit = view.findViewById(R.id.txtProfit);
        lin_deadheat_item = view.findViewById(R.id.lin_deadheat_item);
        lin_deduction = view.findViewById(R.id.lin_deduction);
        edDeduction = view.findViewById(R.id.edDeduction);

    }

    private void getData() {

        edbetAmountD = Double.parseDouble(edbetamount.getText().toString().trim());
        edWinOddD = Double.parseDouble(edWinOdds.getText().toString().trim());

        if (edDeduction.getText().toString().equals("")) {
            edDeductionD = Double.parseDouble("0");
        } else {
            edDeductionD = Double.parseDouble(edDeduction.getText().toString());
        }

        if (edPlaceOdd.getText().toString().equals("")) {
            edPaceoddD = Double.parseDouble("0");
        } else {
            edPaceoddD = Double.parseDouble(edPlaceOdd.getText().toString());
        }
    }

    private void postionDeadHeat()
    {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, heat);
        spin_dead.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "postion").equals("")) {
            Helper.setString(sContext, "postion", "0");
        } else {
            spin_dead.setSelection(Integer.parseInt(Helper.getString(sContext, "postion")),
                    true);
        }
        spin_dead.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadHeatPostion = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                    checkAlertCondition();
                } catch (Exception e) {

                } }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

    private void spinnerPlaces() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, paid);
        spin_paid.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "place").equals("")) {
            Helper.setString(sContext, "place", "0");
        } else {
            spin_paid.setSelection(Integer.parseInt(Helper.getString(sContext, "place")),
                    true); }
        spin_paid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadHeatPlace = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                    checkAlertCondition();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerRunner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, runner);
        spin_runner.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "runner").equals("")) {
            Helper.setString(sContext, "runner", "0");
        } else {
            spin_runner.setSelection(Integer.parseInt(Helper.getString(sContext, "runner")),
                    true);
        }
        spin_runner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadHeatRunner = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                    checkAlertCondition();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerDeadHeat() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, outcome);
        spinner_outcome.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "outcome").equals("")) {
            Helper.setString(sContext, "outcome", "0");
        } else {
            spinner_outcome.setSelection(Integer.parseInt(Helper.getString(sContext, "outcome")),
                    true);
        }
        spinner_outcome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                outcomePosition = String.valueOf(parent.getItemIdAtPosition(position));
                {
                    try {
                        checkCondition();
                    } catch (Exception e) {

                    }
                } }
                @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void checkWinCalcultion() {
        getData();
        Double a = (1 - (0 / 100)) * edbetAmountD;
        Double b = (edWinOddD - 1) * a;
        Double c = b + edbetAmountD;
        Double d = a * (edPaceoddD - 1);
        Double winReturn = d + edbetAmountD + c;
        txtReutrn.setText("" + String.format("%.2f", winReturn));

        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", winReturn - stake));

    }
    private void placedCalcultion() {
        getData();
        Double a = ((1 - (0 / 100)) * edbetAmountD);
        Double b = ((edPaceoddD - 1) * a);
        Double placedReturn = b + edbetAmountD;
        txtReutrn.setText("" + String.format("%.2f", placedReturn));

        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", placedReturn - stake));

    }
    private void checkLossCalcultion() {
        getData();
        Double lossReturn = Double.parseDouble("0");
        txtReutrn.setText("" + String.format("%.2f", lossReturn));
        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", lossReturn - stake));
    }
    private void checkVoidCalcultion() {
        getData();
        Double voidReturn = 2 * edbetAmountD;
        txtReutrn.setText("" + String.format("%.2f", voidReturn));
        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", Double.parseDouble("0")));
    }

    private void getAlertDailog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sContext);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_eachwaysetting, null);
        dialogBuilder.setView(dialogView);
        spinner_deduction = dialogView.findViewById(R.id.spinner_deduction);
        spinner_format = dialogView.findViewById(R.id.spinner_format);
        TextView txt_setting = dialogView.findViewById(R.id.txt_setting);
        deductionSpinner();
        oddFormatSpinner();
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        txt_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Helper.setString(sContext, "deduction", deductionPostion);
                spinner_deduction.setSelection(Integer.parseInt(Helper.getString(sContext, "deduction")), true);

                Helper.setString(sContext, "oddformat", formatPostion);
                spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "oddformat")), true);

                checkAlertCondition();
                alertDialog.dismiss();
            }
        });

        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

    }

    private void checkCondition() {
        if (text.equals("Win")) {
            lin_deadheat_item.setVisibility(View.GONE);
            if (text.equals("Dead Heat") && Helper.getString(sContext, "deduction").equals("1")) {
                checkWinCalcultionFromDeduction();
            } else {
                checkWinCalcultion();
            }
        } else if (text.equals("Placed")) {
            lin_deadheat_item.setVisibility(View.GONE);
            if (text.equals("Placed") && Helper.getString(sContext, "deduction").equals("1")) {
                dedcutionTOPlacedFromSetting();
            } else if (Helper.getString(sContext, "deduction").equals("1") && Helper.getString(sContext, "outcome").equals("2")) {
                dedcutionTOPlacedFromSetting();
            } else {
                placedCalcultion();
            }
        } else if (text.equals("Loss")) {
            lin_deadheat_item.setVisibility(View.GONE);
            checkLossCalcultion();


        } else if (text.equals("Void")) {
            lin_deadheat_item.setVisibility(View.GONE);
            checkVoidCalcultion();

        }

        if (text.equals("Dead Heat")) {
            lin_deadheat_item.setVisibility(View.VISIBLE);
            if (text.equals("Dead Heat") && Helper.getString(sContext, "deduction").equals("1")) {
                deadHeatEachwayDeductionCombination();
            } else {
                deadHeatEachwayPlaceOddCombination();
            }
        }
    }

    private void checkWinCalcultionFromDeduction() {
        getData();
        Double a = (1 - (edDeductionD / 100)) * edbetAmountD;
        Double b = (edWinOddD - 1) * a;
        Double c = b + edbetAmountD;
        Double d = a * (edPaceoddD - 1);
        Double winReturn = d + edbetAmountD + c;
        txtReutrn.setText("" + String.format("%.2f", winReturn));

        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", winReturn - stake));
    }
    private void checkAlertCondition() {
        try {
            if (deductionPostion.equals("1")) {
                if (deductionPostion.equals("1") && Helper.getString(sContext, "outcome").equals("3")) {
                    deadHeatEachwayDeductionCombination();
                } else {
                    simpleConverstionToWinFromDeduction();
                }
            }

            conversionCondtions();
            if (deductionPostion.equals("0")) {
                spinner_outcome.setSelection(0, true);
            }
        } catch (Exception e) {
        }
    }

        private void conversionCondtions() {
        if (Helper.getString(sContext, "oddformat").equals("0"))
        {

          EdittextCheck.decimalFromatConversion(Double.valueOf(String.valueOf(edWinOddD)),edWinOdds);
          EdittextCheck.decimalFromatConversion(Double.valueOf(String.valueOf(edPaceoddD)),edPlaceOdd);

        }  if (Helper.getString(sContext, "oddformat").equals("1")) {

          EdittextCheck.fractinalFormatConversion(Double.valueOf(String.valueOf(edWinOddD)),edWinOdds);
          EdittextCheck.fractinalFormatConversion(Double.valueOf(String.valueOf(edPaceoddD)),edPlaceOdd);

        }  if (Helper.getString(sContext, "oddformat").equals("2")) {
         EdittextCheck.americanFormatConversion(Double.valueOf(String.valueOf(edWinOddD)),edWinOdds);
         EdittextCheck.americanFormatConversion(Double.valueOf(String.valueOf(edPaceoddD)),edPlaceOdd);
        }
        else
            {
            checkCondition(); } }

    private void simpleConverstionToWinFromDeduction() {
        getData();
        Double a = (1 - (edDeductionD / 100)) * edbetAmountD;
        Double b = (edWinOddD - 1) * a;
        Double c = b + edbetAmountD;
        Double d = a * (edPaceoddD - 1);
        Double winReturn = d + edbetAmountD + c;
        txtReutrn.setText("" + String.format("%.2f", winReturn));

        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", winReturn - stake));
    }

    private void deductionSpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, deduction);
        spinner_deduction.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "deduction").equals("")) {
            Helper.setString(sContext, "deduction", "0");
        } else {
            spinner_deduction.setSelection(Integer.parseInt(Helper.getString(sContext, "deduction")),
                    true);
        }
        spinner_deduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                deductionPostion = String.valueOf(parent.getItemIdAtPosition(position));
                if (text.equals("Yes")) {
                    lin_deduction.setVisibility(View.VISIBLE);
                } else if (text.equals("No")) {
                    lin_deduction.setVisibility(View.GONE);
                }
                checkCondition();
                checkAlertCondition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private void oddFormatSpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, format);
        spinner_format.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "oddformat").equals("")) {
            Helper.setString(sContext, "oddformat", "0");
        } else {
            spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "oddformat")),
                    true);
        }
        spinner_format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                formatPostion = String.valueOf(parent.getItemIdAtPosition(position));
                checkCondition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void deadHeatEachwayPlaceOddCombination() {
        getData();
        Double a = ((1 - (0)) * edbetAmountD);
        Double b = (edWinOddD - 1) * a;
        Double returnn = edbetAmountD + b;

        Double profit = a * (edPaceoddD - 1);
        Double returnPrfoit = edbetAmountD + profit;
        Double stake = 2 * edbetAmountD;

        if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") &&   deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 2;
            Double result = returnn / 2;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") &&
                deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 1;
            Double result = returnn / 2;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") &&
                deadHeatRunner.equals("0")) {
            Double p = returnPrfoit / 1;
            Double result = returnn / 2;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") &&
                deadHeatRunner.equals("1")) {

            Double p = returnPrfoit / 3;
            Double result = returnn / 3;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") &&
                deadHeatRunner.equals("1")) {

            Double p = (returnPrfoit * 2) / 3;
            Double result = returnn / 3;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") &&
                deadHeatRunner.equals("1")) {
            Double p = returnPrfoit / 1;
            Double result = returnn / 3;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") &&
                deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 4;

            Double result = returnn / 4;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") &&
                deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 2;
            //  txtProfit.setText("" + String.format("%.2f", p));
            Double result = returnn / 4;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") &&
                deadHeatRunner.equals("2")) {
            Double p = (returnPrfoit * 3) / 4;
            // txtProfit.setText("" + String.format("%.2f", p));
            Double result = returnn / 4;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));
        }


        // second pos


        else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") &&
                deadHeatRunner.equals("0")) {

            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") &&
                deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 2;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") &&
                deadHeatRunner.equals("0")) {
            Double p = returnPrfoit * 1;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") &&
                deadHeatRunner.equals("1")) {

            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") &&
                deadHeatRunner.equals("1")) {
            Double p = returnPrfoit / 3;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") &&
                deadHeatRunner.equals("1")) {

            Double p = returnPrfoit * 2 / 3;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0")
                && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1")
                && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 4;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2")
                && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 2;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        }

        // third pos
        else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0")
                && deadHeatRunner.equals("0")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1")
                && deadHeatRunner.equals("0")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2")
                && deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 2;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0")
                && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1")
                && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2")
                && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit / 3;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {
               Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") &&
                deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 4;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));
        }
    }
    private void dedcutionTOPlacedFromSetting() {
        getData();
        Double a = (1 - (edDeductionD / 100)) * edbetAmountD;
        Double b = ((edPaceoddD - 1) * a);
        Double placedReturn = b + edbetAmountD;
        txtReutrn.setText("" + String.format("%.2f", placedReturn));

        Double stake = edbetAmountD * 2;
        txttotalStake.setText("" + String.format("%.2f", stake));
        txtProfit.setText("" + String.format("%.2f", placedReturn - stake));
    }
    private void deadHeatEachwayDeductionCombination() {
        getData();
        Double a = ((1 - (edDeductionD / 100)) * edbetAmountD);
        Double b = (edWinOddD - 1) * a;
        Double returnn = edbetAmountD + b;

        Double profit = a * (edPaceoddD - 1);
        Double returnPrfoit = edbetAmountD + profit;
        Double stake = 2 * edbetAmountD;

        if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 2;
            Double result = returnn / 2;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 1;
            Double result = returnn / 2;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {
            Double p = returnPrfoit / 1;
            Double result = returnn / 2;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            Double p = returnPrfoit / 3;
            Double result = returnn / 3;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            Double p = (returnPrfoit * 2) / 3;
            Double result = returnn / 3;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit / 1;
            Double result = returnn / 3;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 4;

            Double result = returnn / 4;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 2;
            //  txtProfit.setText("" + String.format("%.2f", p));
            Double result = returnn / 4;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            Double p = (returnPrfoit * 3) / 4;
            // txtProfit.setText("" + String.format("%.2f", p));
            Double result = returnn / 4;
            Double fin = result + p;
            txtReutrn.setText("" + String.format("%.2f", fin));
            txtProfit.setText("" + String.format("%.2f", fin - stake));
        }


        // second pos

        else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {

            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 2;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {
            Double p = returnPrfoit * 1;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit / 3;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            Double p = returnPrfoit * 2 / 3;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 4;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 2;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        }

        // third pos
        else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));

        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {

            Double p = returnPrfoit / 2;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {
            Double p = returnPrfoit / 3;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit * 0;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            Double p = returnPrfoit / 4;
            txtReutrn.setText("" + String.format("%.2f", p));
            txtProfit.setText("" + String.format("%.2f", p - stake));
        }
    }
    public void Fraction(double decimal) {
        String stringNumber = String.valueOf(decimal);
        int numberDigitsDecimals = stringNumber.length() - 1 - stringNumber.indexOf('.');
        denominator = 1;
        for (int i = 0; i < numberDigitsDecimals; i++) {
            decimal *= 10;
            denominator *= 10;
        }
        int numerator = (int) Math.round(decimal);
        int greatestCommonFactor = greatestCommonFactor(numerator, denominator);
        numerator1 = numerator / greatestCommonFactor;
        demonintor1 = denominator / greatestCommonFactor;
    }

    public int greatestCommonFactor(int num, int denom) {
        if (denom == 0) {
            return num;
        }
        return greatestCommonFactor(denom, num % denom);
    }

    private void fractinalFormatConversion() {
        String s = edWinOdds.getText().toString();
        if (edWinOdds.getText().toString().contains("-")) {
            String a = edWinOdds.getText().toString().replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            Fraction(Double.parseDouble(String.format("%.2f", valD)));
            int res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            edWinOdds.setText("" + fractionResult);
        } else if (s.contains("+")) {
            Double val1 = (Double.parseDouble(s) / 100);
            Fraction(val1);
            String fResult = (numerator1) + "/" + demonintor1;
            edWinOdds.setText("" + fResult);              //+oddd to fractionl
        } else {
            Fraction(Double.parseDouble(String.format("%.2f", edWinOddD)));
            res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            edWinOdds.setText(fractionResult);
        }
    }
    private void americanFormatConversion() {
        String ss = edWinOdds.getText().toString();
        if (ss.contains("/")) {
            String[] rat = ss.split("/");
            String n = rat[0];
            String v = rat[1];

            if (Double.valueOf(Double.parseDouble(n)) > Double.valueOf(Double.parseDouble(v))) {
                Double valM = (Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v)) * 100));
                Double a = Double.parseDouble(String.valueOf(valM));
                int i = a.intValue();
                edWinOddD = Double.valueOf(i);
                edWinOdds.setText("+" + "" + i);
            }
            else if (Double.valueOf(Double.parseDouble(n)) < Double.valueOf(Double.parseDouble(v))) {
                Double valm = ((100) / Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v))));
                Double aa = Double.parseDouble(String.valueOf(valm));
                int j = aa.intValue();
                edWinOddD = Double.valueOf(j);
                edWinOdds.setText("-" + "" + j);
            } }

        else {
            if (edWinOddD >= 2) {
                Double valPostive = (edWinOddD - 1) * 100;
                int i = Integer.valueOf(valPostive.intValue());
                edWinOdds.setText("+" + "" + i);
                edWinOddD = Double.valueOf(i); }
            else {
                 Double valNegative = (-100) / (edWinOddD - 1);
                int j = valNegative.intValue();
                edWinOdds.setText("" + j);
                edWinOddD = Double.valueOf(j);
            }
        }
    }


    private void decimalFromatConversion(String value) {
        getData();
        String s = value;

        if (s.contains("/")) {
            String[] rat = s.split("/");
            String numnerator = rat[0];
            String denomintor = rat[1];
            Double valD = (Double.valueOf(numnerator) / Double.parseDouble(denomintor) + 1);
            edWinOdds.setText("" + String.format("%.2f", valD));
            edWinOddD = valD;

        }  if (s.startsWith("-")) {
            String a = s.replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            edWinOdds.setText(String.format("%.2f", valD));   //-odd to decimal
            edWinOddD = valD;
        }  if (s.startsWith("+")) {
            Double val = (Double.parseDouble(s) / 100) + 1;
            edWinOdds.setText(String.valueOf(val));     //+odd to decimal
            edWinOddD = val;
        }
    }
}