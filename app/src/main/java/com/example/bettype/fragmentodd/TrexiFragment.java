package com.example.bettype.fragmentodd;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bettype.R;
import com.example.bettype.adapters.AccumaltorAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TrexiFragment extends Fragment {
    Context sContext;
    @BindView(R.id.spinner_staketype)
    Spinner spinner_staketype;
    @BindView(R.id.recycler_trexi)
    RecyclerView recycler_trexi;
    @BindView(R.id.spinner_format)
    Spinner spinner_format;
    @BindView(R.id.text_no)
    TextView text_no;
    @BindView(R.id.text_yes)
    TextView text_yes;
    String[] fomat = {"Decimal", "Fractional", "American"};
    String[] stake = {"Stake Per Bet", "Total Combined"};
    AccumaltorAdapter adapter;
    List<String> list = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trexi, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        list.add("1");
        list.add("2");
        list.add("3");
        setBackground(text_no,text_yes);
        setTextColor(text_no,text_yes);
        recycler();
        formatSpinner();
        stakeSpinner();
        return view;
    }

    private void stakeSpinner() {
        ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, stake);
        spinner_staketype.setAdapter(heatAdapter);
        spinner_staketype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Stake Per Bet")) {
                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

    private void formatSpinner() {
        ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, fomat);
        spinner_format.setAdapter(heatAdapter);
        spinner_format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Decimal")) {
                } else if (text.equals("Fractional")) {

                } else {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void recycler() {
        recycler_trexi.setLayoutManager(new LinearLayoutManager(sContext));
        adapter = new AccumaltorAdapter(sContext, list);
        recycler_trexi.setAdapter(adapter);
    }

    @OnClick(R.id.text_no)
    public void text_no() {
     setBackground(text_no,text_yes);
     setTextColor(text_no,text_yes);

    }

    @OnClick(R.id.text_yes)
    public void text_yes() {
        setBackground(text_yes,text_no);
        setTextColor(text_yes,text_no);
    }
    public void setBackground(TextView no,TextView yes)
    {
        no.setBackgroundResource(R.color.colorPrimary);
        yes.setBackgroundResource(R.color.colorWhite);
    }

    public void setTextColor(TextView no,TextView yes)
    {
        no.setTextColor(getResources().getColor(R.color.colorWhite));
        yes.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
    }




}
