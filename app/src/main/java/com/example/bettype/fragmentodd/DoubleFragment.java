package com.example.bettype.fragmentodd;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.activities.MainActivity1;
import com.example.bettype.utilies.Global;
import com.example.bettype.utilies.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.bettype.utilies.Helper.hideKeyboard;

public class DoubleFragment extends Fragment {
    Context sContext;
    EditText edbetamount, edOdd, edPlaceodd, edDeduction, edOdd1, edPlaceOdd1, edDeduction1;
    Double edBetAmountD, edOddD, edPlacedOddD, edDeductuonD, edBetStakeD1, edOddD1, edPlacedOddD1, edDeductionD1;
    TextView txtTotalStake, txtReturn, txtProfit;
    Spinner spinnerOne, spinnerTwo, spin_dead, spin_paid, spin_runner, spin_dead1, spin_paid1, spin_runner1;
    LinearLayout lin_spinner_item, lin_spinner_item1;
    private Spinner spinner_eachway, spinner_format, spinner_deduction;
    AlertDialog alertDialog;
    String[] heat = {"First", "Second", "Third",};
    String[] paid = {"One", "Two", "Three",};
    String[] runner = {"Two", "Three", "Four",};
    String[] outcome = {"Win", "Loss", "Placed", "Dead Heat", "Void"};
    String[] deduction = {"No", "Yes"};
    String[] eachway = {"No", "Yes"};
    String[] format = {"Decimal", "Fractional", "American"};
    TextView txtdeduction, txtPlaceodd;
    String deadHeatPostion, deadHeatPlace, deadHeatRunner;
    String deductionPostion, eachwayPostion, formatPostion, outcomePosition;
    ArrayAdapter heatAdapter;
    String spinnerOneText, spinnerTwoText;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_double, container, false);
        sContext = getActivity();
        Helper.clearPref(sContext);
        findView(view);
        spinnerDeadHeat();
        spinnerDeadHeat2();
        Global.spinner(sContext, R.layout.spinner_item, heat, spin_dead);
        Global.spinner(sContext, R.layout.spinner_item, paid, spin_paid);
        Global.spinner(sContext, R.layout.spinner_item, runner, spin_runner);
        MainActivity1.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAlertDailog();
            }
        });
        keyboradListener();
        return view;
    }

    private void keyboradListener() {

        edbetamount.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edbetamount.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edBetAmountD = Double.parseDouble(s);
                            checkCondition();

                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        edOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edOdd.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edOddD = Double.parseDouble(s);
                            checkCondition();  }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });


        edPlaceodd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edPlaceodd.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edPlacedOddD = Double.parseDouble(s);
                            checkCondition(); }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });


        edDeduction.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edDeduction.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edDeductuonD = Double.parseDouble(s);
                            checkCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });


        edOdd1.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edOdd1.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edOddD1 = Double.parseDouble(s);  checkCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });


        edPlaceOdd1.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edPlaceOdd1.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edPlacedOddD1 = Double.parseDouble(s);  checkCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });


        edDeduction1.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edDeduction1.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edDeductionD1 = Double.parseDouble(s);  checkCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });
    }

        private void findView(View view) {

        edbetamount = view.findViewById(R.id.edbetamount);
        edOdd = view.findViewById(R.id.edOdd);
        edPlaceodd = view.findViewById(R.id.edPlaceodd);
        edDeduction = view.findViewById(R.id.edDeduction);
        edOdd1 = view.findViewById(R.id.edOdd1);
        edPlaceOdd1 = view.findViewById(R.id.edPlaceOdd1);
        edDeduction1 = view.findViewById(R.id.edDeduction1);
        txtTotalStake = view.findViewById(R.id.txtTotalStake);
        txtReturn = view.findViewById(R.id.txtReturn);
        txtProfit = view.findViewById(R.id.txtProfit);
        spinnerOne = view.findViewById(R.id.spinnerOne);
        spinnerTwo = view.findViewById(R.id.spinnerTwo);


        spin_dead = view.findViewById(R.id.spin_dead);
        spin_paid = view.findViewById(R.id.spin_paid);
        spin_runner = view.findViewById(R.id.spin_runner);
        spin_dead1 = view.findViewById(R.id.spin_dead1);
        spin_paid1 = view.findViewById(R.id.spin_paid1);
        spin_runner1 = view.findViewById(R.id.spin_runner1);

        lin_spinner_item = view.findViewById(R.id.lin_spinner_item);
        lin_spinner_item1 = view.findViewById(R.id.lin_spinner_item1);

        txtdeduction = view.findViewById(R.id.txtdeduction);
        txtPlaceodd = view.findViewById(R.id.txtPlaceodd);
    }

    private void getValue() {
        edBetAmountD=Double.parseDouble(edbetamount.getText().toString());

        edBetAmountD=Double.parseDouble(edbetamount.getText().toString());
        edOddD=Double.parseDouble(edOdd.getText().toString());
        edOddD1=Double.parseDouble(edOdd1.getText().toString());

        edPlacedOddD=Double.parseDouble(edPlaceodd.getText().toString());
        edPlacedOddD1=Double.parseDouble(edPlaceOdd1.getText().toString());


        edDeductuonD=Double.parseDouble(edDeduction.getText().toString());
        edDeductionD1=Double.parseDouble(edDeduction1.getText().toString());

    }

    private void spinnerDeadHeat() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, outcome);
        spinnerOne.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "outcome").equals("")) {
            Helper.setString(sContext, "outcome", "0");
        } else {
            spinnerOne.setSelection(Integer.parseInt(Helper.getString(sContext, "outcome")),
                    true);
        }
        spinnerOne.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerOneText = parent.getSelectedItem().toString();
                checkCondition();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerDeadHeat2() {
        ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, outcome);
        spinnerTwo.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "outcome1").equals("")) {
            Helper.setString(sContext, "outcome1", "0");
        } else {
            spinnerTwo.setSelection(Integer.parseInt(Helper.getString(sContext, "outcome1")),
                    true);
        }
        spinnerTwo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                spinnerTwoText = parent.getSelectedItem().toString();
                if (spinnerTwoText.equals("Dead Heat")) {
                    lin_spinner_item1.setVisibility(View.VISIBLE);
                } else {
                    lin_spinner_item1.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private void getAlertDailog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sContext);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_oddsetting, null);
        dialogBuilder.setView(dialogView);
        spinner_deduction = dialogView.findViewById(R.id.spinner_deduction);
        spinner_eachway = dialogView.findViewById(R.id.spinner_eachway);
        spinner_format = dialogView.findViewById(R.id.spinner_format);
        TextView txt_setting = dialogView.findViewById(R.id.txt_setting);
        deductionSpinner();
        eachwaySpinner();
        oddFormatSpinner();
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        txt_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Helper.setString(sContext, "deduction", deductionPostion);
                spinner_deduction.setSelection(Integer.parseInt(Helper.getString(sContext, "deduction")), true);
                Helper.setString(sContext, "eachway", eachwayPostion);
                spinner_eachway.setSelection(Integer.parseInt(Helper.getString(sContext, "eachway")), true);
                Helper.setString(sContext, "oddformat", formatPostion);
                spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "oddformat")), true);
                checkAlertCondition();
                alertDialog.dismiss();
            }
        });

        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void checkAlertCondition() {

    }

    private void oddFormatSpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, format);
        spinner_format.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "oddformat").equals("")) {
            Helper.setString(sContext, "oddformat", "0");
        } else {
            spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "oddformat")),
                    true);
        }
        spinner_format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formatPostion = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                //  checkCondition();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private void checkCondition() {

        if (spinnerOneText.equals("Win")) {
            lin_spinner_item.setVisibility(View.GONE);
            winconversion(Double.parseDouble("0"));      //leg1
            Toast.makeText(sContext,"win",Toast.LENGTH_LONG).show();

        } else if (spinnerOneText.equals("Placed")) {
            lin_spinner_item.setVisibility(View.GONE);
            Toast.makeText(sContext,"placed",Toast.LENGTH_LONG).show();
        } else if (spinnerOneText.equals("Loss")) {

            lin_spinner_item.setVisibility(View.GONE);
            Toast.makeText(sContext,"loss",Toast.LENGTH_LONG).show();
           // lossConversion();
        } else if (spinnerOneText.equals("Void")) {
            lin_spinner_item.setVisibility(View.GONE);
            Toast.makeText(sContext,"void",Toast.LENGTH_LONG).show();
        } else if (spinnerOneText.equals("Dead Heat")) {
            lin_spinner_item.setVisibility(View.VISIBLE);
            Toast.makeText(sContext,"heat",Toast.LENGTH_LONG).show();
        }
    }


    private void lossConversion() {
        Double lossReturn = edBetAmountD * Double.parseDouble("0");
        txtReturn.setText(String.format("%.2f", lossReturn));
        Double lossProfit = edBetAmountD * (-1);
        txtProfit.setText(String.format("%.2f", lossProfit));
    }


    private void eachwaySpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, eachway);
        spinner_eachway.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "eachway").equals("")) {
            Helper.setString(sContext, "eachway", "0");
        } else {
            spinner_eachway.setSelection(Integer.parseInt(Helper.getString(sContext, "eachway")),
                    true);
        }
        spinner_eachway.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                eachwayPostion = String.valueOf(parent.getItemIdAtPosition(position));
                if (text.equals("Yes")) {
                    txtPlaceodd.setVisibility(View.VISIBLE);
                    edPlaceodd.setVisibility(View.VISIBLE);
                    edPlaceOdd1.setVisibility(View.VISIBLE);
                } else if (text.equals("No")) {
                    txtPlaceodd.setVisibility(View.GONE);
                    edPlaceodd.setVisibility(View.GONE);
                    edPlaceOdd1.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void deductionSpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, deduction);
        spinner_deduction.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "deduction").equals("")) {
            Helper.setString(sContext, "deduction", "0");
        } else {
            spinner_deduction.setSelection(Integer.parseInt(Helper.getString(sContext, "deduction")),
                    true);
        }
        spinner_deduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                deductionPostion = String.valueOf(parent.getItemIdAtPosition(position));
                if (text.equals("Yes")) {

                    txtdeduction.setVisibility(View.VISIBLE);
                    edDeduction.setVisibility(View.VISIBLE);
                    edDeduction1.setVisibility(View.VISIBLE);
                } else if (text.equals("No")) {
                    txtdeduction.setVisibility(View.GONE);
                    edDeduction.setVisibility(View.GONE);
                    edDeduction1.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    private void winconversion(Double ded) {
        Double a= (ded - 1) * edBetAmountD;
        Double b=a*(edOddD-1);
        Double winReturnLeg=b+edBetAmountD;
        Double stake = edBetAmountD * 2;
        txtTotalStake.setText("" + String.format("%.2f", stake));

        Double a1=(1-(edDeductionD1/100))*edBetAmountD;
        Double b1=a1*(edOddD1-1);
        Double returnn=winReturnLeg+b1;
        txtReturn.setText("" + String.format("%.2f", returnn));
        txtProfit.setText("" + String.format("%.2f", returnn-stake));



    }

}