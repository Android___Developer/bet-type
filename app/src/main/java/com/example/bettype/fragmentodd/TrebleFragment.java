package com.example.bettype.fragmentodd;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.utilies.Global;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrebleFragment extends Fragment {
    Context sContext;
    @BindView(R.id.spin_one)
    Spinner spin_one;
    @BindView(R.id.spin_two)
    Spinner spin_two;
    @BindView(R.id.spin_three)
    Spinner spin_three;
    @BindView(R.id.lin_spinner_item)
    LinearLayout lin_spinner_item;
    @BindView(R.id.lin_spinner_item1)
    LinearLayout lin_spinner_item1;
    @BindView(R.id.lin_spinner_item2)
    LinearLayout lin_spinner_item2;
    @BindView(R.id.spin_dead)
    Spinner spin_dead;
    @BindView(R.id.spin_paid)
    Spinner spin_paid;
    @BindView(R.id.spin_runner)
    Spinner spin_runner;


    String[] heat = {"First","Second", "Third",};
    String[] paid = {"One","Two", "Three",};
    String[] runner = {"Two","Three", "Four",};
    String[] outcome = {"Win", "Loss", "Placed", "Dead Heat","Void"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_treble, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        spinnerDeadHeat();
        spinnerDeadHeat2();
        spinnerDeadHeat3();
        Global.spinner(sContext,R.layout.spinner_item,heat,spin_dead);
        Global.spinner(sContext,R.layout.spinner_item,paid,spin_paid);
        Global.spinner(sContext,R.layout.spinner_item,runner,spin_runner);
        return view;
    }

    private void spinnerDeadHeat() {
        ArrayAdapter heatAdapter=new ArrayAdapter(sContext,R.layout.spinner_item, R.id.text,outcome );
        spin_one.setAdapter(heatAdapter);
        spin_one.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Dead Heat")) {
                    lin_spinner_item.setVisibility(View.VISIBLE); }
                else {
                    lin_spinner_item.setVisibility(View.GONE); } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

    private void spinnerDeadHeat2() {
        ArrayAdapter heatAdapter=new ArrayAdapter(sContext,R.layout.spinner_item, R.id.text,outcome );
        spin_two.setAdapter(heatAdapter);
        spin_two.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Dead Heat")) {
                    lin_spinner_item1.setVisibility(View.VISIBLE); }
                else {
                    lin_spinner_item1.setVisibility(View.GONE); } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

    private void spinnerDeadHeat3() {
        ArrayAdapter heatAdapter=new ArrayAdapter(sContext,R.layout.spinner_item, R.id.text,outcome );
        spin_three.setAdapter(heatAdapter);
        spin_three.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Dead Heat")) {
                    lin_spinner_item2.setVisibility(View.VISIBLE); }
                else {
                    lin_spinner_item2.setVisibility(View.GONE); } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });}
}