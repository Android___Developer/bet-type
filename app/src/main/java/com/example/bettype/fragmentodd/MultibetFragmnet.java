package com.example.bettype.fragmentodd;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.adapters.MultibetAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MultibetFragmnet extends Fragment {
    Context sContext;
    @BindView(R.id.reyclerview)
    RecyclerView reyclerview;
    List<String> list = new ArrayList<>();
    MultibetAdapter adapter;
    @BindView(R.id.txt_minus)
    TextView txt_minus;
    @BindView(R.id.txt_plus)
    TextView txt_plus;
    @BindView(R.id.txt_count)
    TextView txt_count;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_multibet, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        list.add("1");
        list.add("2");
        recyler();
        return view;
    }

    private void recyler() {
        reyclerview.setLayoutManager(new LinearLayoutManager(sContext));
        adapter = new MultibetAdapter(sContext, list);
        reyclerview.setAdapter(adapter);
        reyclerview.setNestedScrollingEnabled(false);
        adapter.onItemCLickListener(new MultibetAdapter.itemClickInterface() {
            @Override
            public void onItemClick(int pos) {

            }
        });
    }

    @OnClick(R.id.txt_plus)
    public void txt_plus() {
        if (list.size() > 1 && list.size() < 15) {
            int i = list.size() + 1;
            list.add(i + "");
            txt_count.setText("" + i);
            adapter.notifyItemInserted(i);
            scrollToBottom();
        } else {
        } }

    private void scrollToBottom() {
        reyclerview.scrollToPosition(list.size() - 1); }

    @OnClick(R.id.txt_minus)
    public void txt_minus() {
        Log.e("size", list.size() + "");
        if (list.size() > 2) {
            list.remove(list.size() - 1);
            adapter.notifyItemRemoved(list.size());
            txt_count.setText("" + list.size());
            scrollToBottom();
        } else {
            Toast.makeText(sContext, "can't delet", Toast.LENGTH_SHORT).show(); }
    }
}



