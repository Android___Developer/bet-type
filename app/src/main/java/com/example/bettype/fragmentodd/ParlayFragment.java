package com.example.bettype.fragmentodd;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.adapters.AccumaltorAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ParlayFragment extends Fragment {
    Context sContext;

    //    @BindView(R.id.lin_spinner_item)
//    LinearLayout lin_spinner_item;
    @BindView(R.id.spinner_rule)
    Spinner spinner_rule;
    @BindView(R.id.spinner_format)
    Spinner spinner_format;
    @BindView(R.id.recycler_parlay)
    RecyclerView recycler_parlay;
    String[] fomat={"Decimal","Fractional","American"};
    String[] rule={"Yes","No"};
    AccumaltorAdapter adapter;
    List<String> list=new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parlay, container, false);
        sContext = getActivity();
        ButterKnife.bind(this,view);
        list.add("1");
        list.add("2");
        list.add("3");
        recycler();
        formatSpinner();
        ruleSpinner();
        return view;
    }

    private void ruleSpinner() {
        ArrayAdapter heatAdapter=new ArrayAdapter(sContext,R.layout.spinner_item, R.id.text,rule );
        spinner_rule.setAdapter(heatAdapter);
        spinner_rule.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Yes")) {
                }
                else {
                } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

    private void formatSpinner() {
        ArrayAdapter heatAdapter=new ArrayAdapter(sContext,R.layout.spinner_item, R.id.text,fomat );
        spinner_format.setAdapter(heatAdapter);
        spinner_format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Decimal")) {
                }
                else if (text.equals("Fractional"))
                {

                }
                else {
                } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void recycler() {
        recycler_parlay.setLayoutManager(new LinearLayoutManager(sContext));
        adapter=new AccumaltorAdapter(sContext,list);
        recycler_parlay.setAdapter(adapter);
    }
    @OnClick(R.id.text_no)
    public void text_no()
    {

    }

    @OnClick(R.id.text_yes)
    public void text_yes()
    {

    }
}

