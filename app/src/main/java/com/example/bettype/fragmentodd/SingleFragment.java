package com.example.bettype.fragmentodd;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.activities.MainActivity1;
import com.example.bettype.utilies.EdittextCheck;
import com.example.bettype.utilies.Helper;

import static com.example.bettype.utilies.Helper.hideKeyboard;

public class SingleFragment extends Fragment {
    Context sContext;
    AlertDialog alertDialog;
    private LinearLayout lin_spinner_item, lin_dedcution, lin_place_odd;
    private Spinner spinner_outcome, spin_dead, spin_paid, spin_runner;
    private EditText ed_betstake, ed_odd, ed_Deduction, edPlaceOdds;
    private TextView txt_profit, txt_return;
    Double edBetStakeD, edOddD, edDeductionD, edPlaceOddsD;
    String text;
    String[] deduction = {"No", "Yes"};
    String[] eachway = {"No", "Yes"};
    String[] format = {"Decimal", "Fractional", "American"};
    String[] heat = {"First", "Second", "Third",};
    String[] paid = {"One", "Two", "Three",};
    String[] runner = {"Two", "Three", "Four",};
    String[] outcome = {"Win", "Loss", "Placed", "Dead Heat", "Void"};
    private Spinner spinner_eachway, spinner_format, spinner_deduction;
    ArrayAdapter heatAdapter;
    RelativeLayout relativeBackgorund;
    String deadHeatPostion, deadHeatPlace, deadHeatRunner;
    String deductionPostion, eachwayPostion, formatPostion, outcomePosition;
    private int numerator1, demonintor1;
    private String fractionResult = "";
    private int res;
    int denominator = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_single, container, false);
        sContext = getActivity();
        Helper.clearPref(sContext);
        findView(view);
        spinnerDeadHeat();
        postionDeadHeat();
        spinnerPlaces();
        spinnerRunner();
        MainActivity1.setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAlertDailog();
            }
        });
        keyBoardDoneListener();
        return view;
    }

    private void postionDeadHeat() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, heat);
        spin_dead.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "postion").equals("")) {
            Helper.setString(sContext, "postion", "0");
        } else {
            spin_dead.setSelection(Integer.parseInt(Helper.getString(sContext, "postion")),
                    true);
        }
        spin_dead.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadHeatPostion = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerPlaces() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, paid);
        spin_paid.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "place").equals("")) {
            Helper.setString(sContext, "place", "0");
        } else {
            spin_paid.setSelection(Integer.parseInt(Helper.getString(sContext, "place")),
                    true);
        }
        spin_paid.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadHeatPlace = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerRunner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, runner);
        spin_runner.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "runner").equals("")) {
            Helper.setString(sContext, "runner", "0");
        } else {
            spin_runner.setSelection(Integer.parseInt(Helper.getString(sContext, "runner")),
                    true);
        }
        spin_runner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                deadHeatRunner = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void findView(View view) {
        spinner_outcome = view.findViewById(R.id.spinner_outcome);
        spin_dead = view.findViewById(R.id.spin_dead);
        spin_paid = view.findViewById(R.id.spin_paid);
        spin_runner = view.findViewById(R.id.spin_runner);
        lin_spinner_item = view.findViewById(R.id.lin_spinner_item);
        lin_dedcution = view.findViewById(R.id.lin_dedcution);
        lin_place_odd = view.findViewById(R.id.lin_place_odd);
        ed_betstake = view.findViewById(R.id.ed_betstake);
        lin_place_odd = view.findViewById(R.id.lin_place_odd);
        ed_odd = view.findViewById(R.id.ed_odd);
        txt_profit = view.findViewById(R.id.txt_profit);
        txt_return = view.findViewById(R.id.txt_return);
        ed_Deduction = view.findViewById(R.id.ed_Deduction);
        edPlaceOdds = view.findViewById(R.id.edPlaceOdds);
        relativeBackgorund = view.findViewById(R.id.relativeBackgorund);
    }

    private void getEdittextData() {
        edBetStakeD = Double.parseDouble(ed_betstake.getText().toString().trim());
        if (ed_Deduction.getText().toString().equals("")) {
            edDeductionD = Double.parseDouble("0");
        } else {
            edDeductionD = Double.parseDouble(ed_Deduction.getText().toString()); }

        if (edPlaceOdds.getText().toString().equals("")) {
            edPlaceOddsD = Double.parseDouble("0");
        } else {
            edPlaceOddsD = Double.parseDouble(edPlaceOdds.getText().toString().trim());
        }
    }

    private void spinnerDeadHeat() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, outcome);
        spinner_outcome.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "outcome").equals("")) {
            Helper.setString(sContext, "outcome", "0");
        } else {
            spinner_outcome.setSelection(Integer.parseInt(Helper.getString(sContext, "outcome")),
                    true); }
        spinner_outcome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                outcomePosition = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                } catch (Exception e) {
                } }
                @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private void getAlertDailog() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sContext);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.fragment_oddsetting, null);
        dialogBuilder.setView(dialogView);
        spinner_deduction = dialogView.findViewById(R.id.spinner_deduction);
        spinner_eachway = dialogView.findViewById(R.id.spinner_eachway);
        spinner_format = dialogView.findViewById(R.id.spinner_format);
        TextView txt_setting = dialogView.findViewById(R.id.txt_setting);
        deductionSpinner();
        eachwaySpinner();
        oddFormatSpinner();
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        txt_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.setString(sContext, "deduction", deductionPostion);
                spinner_deduction.setSelection(Integer.parseInt(Helper.getString(sContext, "deduction")), true);

                Helper.setString(sContext, "eachway", eachwayPostion);
                spinner_eachway.setSelection(Integer.parseInt(Helper.getString(sContext, "eachway")), true);

                Helper.setString(sContext, "oddformat", formatPostion);
                spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "oddformat")), true);

                checkAlertCondition();
                alertDialog.dismiss();
            }
        });

        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }

    private void checkAlertCondition() {
        try {
            if (deductionPostion.equals("0") && eachwayPostion.equals("1")) {
                if (deductionPostion.equals("0") && eachwayPostion.equals("1") && Helper.getString(sContext, "outcome").equals("3")) {
                    singleEachwayDeadheatCheck(edDeductionD);
                } else {
                    singelEachwayConversionToWin();
                }
            } else if (deductionPostion.equals("1") && eachwayPostion.equals("0")) {

                if (Helper.getString(sContext, "outcome").equals("3")) {
                    deaductionDeadheatCheck();
                } else {
                    deductionConversionToWin();
                }

            } else if (deductionPostion.equals("1") && eachwayPostion.equals("1")) {
                if (deductionPostion.equals("1") && eachwayPostion.equals("1") &&
                        Helper.getString(sContext, "outcome").equals("3")) {
                    singleEachwayDeadheatCheck(edDeductionD);
                } else {
                    deductionEachwayBothConversion();
                }
            }  if (Helper.getString(sContext, "oddformat").equals("0")) {
                decimalFromatConversion();

            } else if (Helper.getString(sContext, "oddformat").equals("1")) {
                fractinalFormatConversion();

            } else if (Helper.getString(sContext, "oddformat").equals("2")) {
                americanFormatConversion();
            } else {
                checkCondition();
            }

            if (deductionPostion.equals("0")&&eachwayPostion.equals("0")) {
                spinner_outcome.setSelection(0, true);
           }


            if (deductionPostion.equals("1")&&eachwayPostion.equals("1")) {
                spinner_outcome.setSelection(0, true);
            }

            if (deductionPostion.equals("0")) {
                spinner_outcome.setSelection(0, true);
            }

            if (eachwayPostion.equals("0")) {
                spinner_outcome.setSelection(0, true);
            }


        } catch (Exception e) {

        }
    }

    private void oddFormatSpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, format);
        spinner_format.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "oddformat").equals("")) {
            Helper.setString(sContext, "oddformat", "0");
        } else {
            spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "oddformat")),
                    true);
        }
        spinner_format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                formatPostion = String.valueOf(parent.getItemIdAtPosition(position));
                try {
                    checkCondition();
                } catch (Exception e) {
                } }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void eachwaySpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, eachway);
        spinner_eachway.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "eachway").equals("")) {
            Helper.setString(sContext, "eachway", "0");
        } else {
            spinner_eachway.setSelection(Integer.parseInt(Helper.getString(sContext, "eachway")),
                    true);
        }
        spinner_eachway.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                eachwayPostion = String.valueOf(parent.getItemIdAtPosition(position));
                if (text.equals("Yes")) {
                    lin_place_odd.setVisibility(View.VISIBLE);
                } else if (text.equals("No")) {
                    lin_place_odd.setVisibility(View.GONE);
                } }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void deductionSpinner() {
        heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, deduction);
        spinner_deduction.setAdapter(heatAdapter);
        if (Helper.getString(sContext, "deduction").equals("")) {
            Helper.setString(sContext, "deduction", "0");
        } else {
            spinner_deduction.setSelection(Integer.parseInt(Helper.getString(sContext, "deduction")),
                    true);
        }
        spinner_deduction.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                deductionPostion = String.valueOf(parent.getItemIdAtPosition(position));
                if (text.equals("Yes")) {
                    lin_dedcution.setVisibility(View.VISIBLE);
                } else if (text.equals("No")) {
                    lin_dedcution.setVisibility(View.GONE);
                } }
                @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void keyBoardDoneListener() {
        ed_betstake.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = ed_betstake.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edBetStakeD = Double.parseDouble(s);
                            checkCondition();
                            checkAlertCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        ed_odd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = ed_odd.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edOddD = Double.parseDouble(s);
                            checkCondition();
                            checkAlertCondition();
                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        ed_Deduction.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = ed_Deduction.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edDeductionD = Double.parseDouble(s);
                            checkAlertCondition();
                            checkCondition();

                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });

        edPlaceOdds.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String s = edPlaceOdds.getText().toString().trim();
                    try {
                        if (s.length() > 0) {
                            hideKeyboard((Activity) sContext);
                            edPlaceOddsD = Double.parseDouble(s);
                            checkAlertCondition();
                            checkCondition();

                        }
                    } catch (Exception e) {
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void winConversion() {
        getEdittextData();
        edBetStakeD = Double.parseDouble(ed_betstake.getText().toString().trim());
        edOddD = Double.parseDouble(ed_odd.getText().toString().trim());
        Double winReturn = edBetStakeD * edOddD;
        Double winProfit = winReturn - edBetStakeD;
        txt_return.setText(String.format("%.2f", winReturn));
        txt_profit.setText(String.format("%.2f", winProfit));
    }

    private void lossConversion() {
        getEdittextData();
        Double lossReturn = edBetStakeD * Double.parseDouble("0");
        txt_return.setText(String.format("%.2f", lossReturn));
        Double lossProfit = edBetStakeD * (-1);
        txt_profit.setText(String.format("%.2f", lossProfit));
    }
    private void placedConversion() {
        getEdittextData();
        Double placeReturn = edBetStakeD * Double.parseDouble("2");
        txt_return.setText(String.format("%.2f", (0) * placeReturn));
        txt_profit.setText(String.format("%.2f", (-1) * (edBetStakeD)));
    }
    private void voidConversion() {
        getEdittextData();
        Double voidReturn = edBetStakeD;
        txt_return.setText(String.format("%.2f", voidReturn));
        Double voidProfit = edBetStakeD * Double.parseDouble("0");
        txt_profit.setText(String.format("%.2f", voidProfit));
    }
    private void singelEachwayConversionToWin() {
        getEdittextData();
        Double returnWin = (edBetStakeD * (edPlaceOddsD + edOddD));
        txt_return.setText(String.format("%.2f", returnWin));
        Double winProfit = (returnWin - (2 * edBetStakeD));
        txt_profit.setText("" + winProfit);
    }
    private void singleEachConversionToVoid() {
        //if eachway yes then void
        getEdittextData();
        Double a = edBetStakeD * 2;
        txt_return.setText(String.format("%.2f", a));
        txt_profit.setText("" + Double.parseDouble("0"));
    }
    private void singleEachwayConversionToPlaced() {
         getEdittextData();
        //if eachway yes then placed
        Double aa = edBetStakeD * edPlaceOddsD;
        txt_return.setText(String.format("%.2f", aa));
        Double bb = aa - (2 * edBetStakeD);
        txt_profit.setText("" + bb);
    }

    private void singleEachwayConversionToLoss() {
        //if eachway yes then loss
        getEdittextData();
        Double aa = 2 * edBetStakeD;
        txt_profit.setText(String.format("%.2f", (-1) * aa));
        txt_return.setText("" + Double.parseDouble("0"));
    }

    private void deductionConversionToWin() {
        getEdittextData();
        //If  only Deduction "YES"
        Double a = (1 - (edDeductionD / 100));
        Double bb = a * edBetStakeD;
        Double b = (edOddD - 1) * (bb);
        Double deductionResutlt = (edBetStakeD + (b));
        txt_return.setText(String.format("%.2f", deductionResutlt));
        txt_profit.setText(String.format("%.2f", deductionResutlt - edBetStakeD));
    }

    private void deductionEachwayBothConversion() {
        getEdittextData();
        //both eachway and placed
        Double aa = ((1 - (edDeductionD / 100)) * edBetStakeD);
        Double bb = (edOddD - 1) * aa;
        Double cc = edBetStakeD + bb;
        Double dd = aa;
        Double ee = (edPlaceOddsD - 1);
        Double ff = (ee * dd) + edBetStakeD;
        Double deductionEachwayResult = ff + cc;
        txt_return.setText(String.format("%.2f", deductionEachwayResult));
        Double deductionEachwayProfit = (deductionEachwayResult) - (edBetStakeD * 2);
        txt_profit.setText(String.format("%.2f", deductionEachwayProfit));

    }

    private void deductionFromSettingToPlaced() {
        getEdittextData();
        //if deduction is yes in placed
//
//        Double a = ((1 - (edDeductionD / 100)) * edBetStakeD);
//        Double b = (a * (edPlaceOddsD - 1));
//        Double deductionResult = b + edBetStakeD;
//        txt_return.setText(String.format("%.2f", deductionResult));
//        txt_profit.setText(String.format("%.2f", deductionResult - (2 * edBetStakeD)));


        Double a = (1 - (edDeductionD / 100)) * edBetStakeD;
        Double b = ((edPlaceOddsD - 1) * a);
        Double placedReturn = b + edBetStakeD;
        txt_return.setText("" + String.format("%.2f", placedReturn));
        Double stake = edBetStakeD * 1;
        txt_profit.setText("" + String.format("%.2f", placedReturn - stake));
    }
    private void deductionEachwayBothPlaced() {
        getEdittextData();

//        Double placedReturn = ((edBetStakeD * edPlaceOddsD - (edPlaceOddsD * edDeductionD) +
//                edDeductionD));
//
        Double a=(1-edDeductionD/100)*edBetStakeD;
        Double b=a*(edPlaceOddsD-1);
        Double placedReturn=b+edBetStakeD;

        txt_profit.setText(String.format("%.2f", placedReturn-(2*edBetStakeD)));
             txt_return.setText(String.format("%.2f",placedReturn));
    }
    private void deductionEachwayBothVoid() {
        getEdittextData();
        Double voidReturn = (2 * edBetStakeD);
        txt_return.setText(String.format("%.2f", voidReturn));
        txt_profit.setText(String.format("%.2f", Double.parseDouble(String.valueOf((voidReturn * (0))))));
    }
    private void deductionEachwayBothLoss() {
        Double lossReturn = (edBetStakeD * 2);
        txt_return.setText(String.format("%.2f", Double.parseDouble(String.valueOf(lossReturn * (0)))));
        txt_profit.setText(String.format("%.2f", (lossReturn) * (-1)));
    }

    private void checkCondition() {
     if (text.equals("Win")) {
            lin_spinner_item.setVisibility(View.GONE);
            if (text.equals("Win")&&Helper.getString(sContext, "deduction").equals("1") &&
                    Helper.getString(sContext, "eachway").equals("0")) {
                deductionConversionToWin();
            }
            else if (text.equals("Win")&&Helper.getString(sContext,"deduction").equals("1") &&
                    Helper.getString(sContext, "eachway").equals("1")) {
                deductionEachwayBothConversion();
            }
            else if (text.equals("Win")&&Helper.getString(sContext, "deduction").equals("0") &&
                    Helper.getString(sContext, "eachway").equals("1")) {
                singelEachwayConversionToWin();
            } else {
                winConversion();
            }
        }
     else if (text.equals("Loss")) {
            lin_spinner_item.setVisibility(View.GONE);
            if (Helper.getString(sContext, "deduction").equals("1")
                    && Helper.getString(sContext, "eachway").equals("1")) {
                deductionEachwayBothLoss();
            }
            else if (Helper.getString(sContext, "deduction").equals("0")
                    && Helper.getString(sContext, "eachway").equals("1")) {
                singleEachwayConversionToLoss();
            }
            else {
                lossConversion();
            }

        } else if (text.equals("Placed")) {
            lin_spinner_item.setVisibility(View.GONE);
            if (text.equals("Placed")&&Helper.getString(sContext, "deduction").equals("1")
                    && Helper.getString(sContext, "eachway").equals("0")) {
                deductionFromSettingToPlaced();
            }
            else if (text.equals("Placed")&& Helper.getString(sContext, "deduction").equals("1")
                    && Helper.getString(sContext, "eachway").equals("1")) {
                deductionEachwayBothPlaced();
            }
            else if (Helper.getString(sContext, "deduction").equals("0")
                    && Helper.getString(sContext, "eachway").equals("1")) {
                singleEachwayConversionToPlaced();
            } else
            {
                placedConversion();
            }
        } else if (text.equals("Void")) {
            lin_spinner_item.setVisibility(View.GONE);

            if (Helper.getString(sContext, "deduction").equals("1")
                    && Helper.getString(sContext, "eachway").equals("1")) {
                deductionEachwayBothVoid();
            }
            else if (Helper.getString(sContext, "deduction").equals("0")
                    && Helper.getString(sContext, "eachway").equals("1"))
            {
                singleEachConversionToVoid();
            }
            else
                voidConversion();
        }

        if (text.equals("Dead Heat")) {
            lin_spinner_item.setVisibility(View.VISIBLE);
            if (text.equals("Dead Heat") && Helper.getString(sContext, "deduction").equals("1")
                    && Helper.getString(sContext, "eachway").equals("0")) {
                deaductionDeadheatCheck();        //only deduction
            } else if (text.equals("Dead Heat") && Helper.getString(sContext, "eachway").equals("1")) {
                singleEachwayDeadheatCheck(edDeductionD);

            } else if (text.equals("Dead Heat") && Helper.getString(sContext, "eachway").equals("1") &&
                    Helper.getString(sContext, "deduction").equals("1")) {
                singleEachwayDeadheatCheck(edDeductionD);
            } else {
                allDeadheatCombinationCheck();
            }
        }
    }

    private void deadHeatConversionByZero() {
        txt_return.setText(String.format("%.2f", Double.parseDouble("0")));
        txt_profit.setText(String.format("%.2f", (-1) * (edBetStakeD)));
    }
    private void deaductionDeadheatCheck() {
        getEdittextData();
        if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {
            deductionConversionToDeadheatByTwo();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {

            deductionConversionToDeadheatByTwo();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            deductionConversionToDeadheatByTwo();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            deductionConversionToDeadheatByThree();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            deductionConversionToDeadheatByThree();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            deductionConversionToDeadheatByThree();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {

            deductionConversionToDeadheatByFour();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            deductionConversionToDeadheatByFour();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            deductionConversionToDeadheatByFour();
        }

        // second pos
        else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {
           // Toast.makeText(sContext, "110  divide 0", Toast.LENGTH_SHORT).show();
            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {
            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            deadHeatConversionByZero();
        }

        // third pos
        else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            deadHeatConversionByZero();
        }
    }
    private void deadheatConversion() {
        getEdittextData();
        Double temp = edBetStakeD * edOddD;
        Double deadHeatReturn = temp / 2;
        txt_return.setText(String.format("%.2f", deadHeatReturn));
        Double deadHeatProfit = deadHeatReturn - edBetStakeD;
        //  Double deadHeatProfit = temp2 - deadHeatReturn;
        txt_profit.setText(String.format("%.2f", deadHeatProfit));
    }
    private void deadheatConversionByFour() {
        getEdittextData();
        Double temp = edBetStakeD * edOddD;
        Double deadHeatReturn = temp / 4;
        txt_return.setText(String.format("%.2f", deadHeatReturn));
        Double deadHeatProfit = deadHeatReturn - edBetStakeD;
        //  Double deadHeatProfit = temp2 - deadHeatReturn;
        txt_profit.setText(String.format("%.2f", deadHeatProfit));
    }
    private void deadheatConversionByThree() {
        getEdittextData();
        Double temp = edBetStakeD * edOddD;
        Double deadHeatReturn = temp / 3;
        txt_return.setText(String.format("%.2f", deadHeatReturn));
        Double deadHeatProfit = deadHeatReturn - edBetStakeD;
        //  Double deadHeatProfit = temp2 - deadHeatReturn;
        txt_profit.setText(String.format("%.2f", deadHeatProfit));
    }

    private void allDeadheatCombinationCheck() {
        if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {
            deadheatConversion();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            deadheatConversion();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {

            deadheatConversion();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            deadheatConversionByThree();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            deadheatConversionByThree();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            deadheatConversionByThree();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            deadheatConversionByFour();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {

            deadheatConversionByFour();
        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            deadheatConversionByFour();
        }


        // second pos
        else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {
         //   Toast.makeText(sContext, "110  divide 0", Toast.LENGTH_SHORT).show();
            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {
            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        }

        // third pos
        else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {

            deadHeatConversionByZero();
        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            deadHeatConversionByZero();
        }
    }
    private void deductionConversionToDeadheatByTwo() {
        getEdittextData();
        //If  only Deduction "YES" then dead heat
        Double a = (1 - edDeductionD / 100) * edBetStakeD;
        Double b = (edOddD - 1) * (a);
        Double c = edBetStakeD + b;
        Double deductionResutlt = c / 2;

        txt_return.setText(String.format("%.2f", deductionResutlt));
        txt_profit.setText(String.format("%.2f", deductionResutlt - edBetStakeD));
    }
    private void deductionConversionToDeadheatByThree() {
        getEdittextData();
        //If  only Deduction "YES" then dead heat
        Double a = (1 - edDeductionD / 100) * edBetStakeD;
        Double b = (edOddD - 1) * (a);
        Double c = edBetStakeD + b;
        Double deductionResutlt = c / 3;
        txt_return.setText(String.format("%.2f", deductionResutlt));
        txt_profit.setText(String.format("%.2f", deductionResutlt - edBetStakeD));
    }
    private void deductionConversionToDeadheatByFour() {
        getEdittextData();
        //If  only Deduction "YES" then dead heat
        Double a = (1 - edDeductionD / 100) * edBetStakeD;
        Double b = (edOddD - 1) * (a);
        Double c = edBetStakeD + b;
        Double deductionResutlt = c / 4;
        txt_return.setText(String.format("%.2f", deductionResutlt));
        txt_profit.setText(String.format("%.2f", deductionResutlt - edBetStakeD));
    }
    private void singleEachwayDeadheatCheck(Double deduction) {
        getEdittextData();
        Double stake = 2 * edBetStakeD;
        Double a = ((1 - (deduction / 100)) * edBetStakeD);
        Double b = (edOddD - 1) * a;
        Double aa = ((1 - (deduction / 100)) * edBetStakeD);
        Double bb = (edPlaceOddsD - 1) * aa;

        if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) / 2;  //profit
            Double returnProfit = (bb + edBetStakeD) / 2;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) / 2;  //profit
            Double returnProfit = (bb + edBetStakeD) / 1;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin)));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) / 2;  //profit
            Double returnProfit = (bb + edBetStakeD) / 1;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) / 3;  //profit
            Double returnProfit = (bb + edBetStakeD) / 3;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) / 3;  //profit
            Double returnProfit = (bb + edBetStakeD) * 2 / 3;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) / 3;  //profit
            Double returnProfit = (bb + edBetStakeD) / 1;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {

            Double returnWin = (b + edBetStakeD) / 4;  //profit
            Double returnProfit = (bb + edBetStakeD) / 4;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {
            Double returnWin = (b + edBetStakeD) / 4;  //profit
            Double returnProfit = (bb + edBetStakeD) / 2;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("0") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            Double returnWin = (b + edBetStakeD) / 4;  //profit
            Double returnProfit = (bb + edBetStakeD) * 3 / 4;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));
        }


        // second pos
        else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 2;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 1;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 6;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 2 / 3;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 4;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("1") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {

            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 2;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        }

        // third pos
        else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("0")) {

            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("0")) {

            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("0")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 2;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("1")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 2;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("0") && deadHeatRunner.equals("2")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("1") && deadHeatRunner.equals("2")) {

            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) * 0;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));


        } else if (deadHeatPostion.equals("2") && deadHeatPlace.equals("2") && deadHeatRunner.equals("2")) {
            Double returnWin = (b + edBetStakeD) * 0;  //profit
            Double returnProfit = (bb + edBetStakeD) / 4;     // for win
            txt_return.setText(String.format("%.2f", returnWin + returnProfit));
            txt_profit.setText(String.format("%.2f", (returnWin + returnProfit - stake)));

        }
    }
    private void fractinalFormatConversion() {
        String s = ed_odd.getText().toString();
        if (ed_odd.getText().toString().contains("-")) {
            String a = ed_odd.getText().toString().replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            Fraction(Double.parseDouble(String.format("%.2f", valD)));
            int res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            ed_odd.setText("" + fractionResult);
        } else if (s.contains("+")) {
            Double val1 = (Double.parseDouble(s) / 100);
            Fraction(val1);
            String fResult = (numerator1) + "/" + demonintor1;
            ed_odd.setText("" + fResult);              //+oddd to fractionl
        } else {
            Fraction(Double.parseDouble(String.format("%.2f", edOddD)));
            res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            ed_odd.setText(fractionResult);
        }


    }
    private void americanFormatConversion() {
        String ss = ed_odd.getText().toString();
        if (ss.contains("/")) {
            String[] rat = ss.split("/");
            String n = rat[0];
            String v = rat[1];

            if (Double.valueOf(Double.parseDouble(n)) > Double.valueOf(Double.parseDouble(v))) {
                Double valM = (Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v)) * 100));
                Double a = Double.parseDouble(String.valueOf(valM));
                int i = a.intValue();
                // americanConversin(Double.valueOf(i));
                ed_odd.setText("+" + "" + i);
            } else if (Double.valueOf(Double.parseDouble(n)) < Double.valueOf(Double.parseDouble(v))) {
                Double valm = ((100) / Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v))));
                Double aa = Double.parseDouble(String.valueOf(valm));
                int j = aa.intValue();
                //   americanConversin(Double.valueOf(j));
                ed_odd.setText("-" + "" + j);
            }

        } else {
            if (edOddD >= 2) {
                Double valPostive = (edOddD - 1) * 100;
                int i = Integer.valueOf(valPostive.intValue());
                ed_odd.setText("+" + "" + i);
                //   americanConversin(Double.valueOf(i));

            } else {
                Double valNegative = (-100) / (edOddD - 1);
                int j = valNegative.intValue();
                ed_odd.setText("" + j);
            }
        }
    }
    public void Fraction(double decimal) {
        String stringNumber = String.valueOf(decimal);
        int numberDigitsDecimals = stringNumber.length() - 1 - stringNumber.indexOf('.');
        denominator = 1;
        for (int i = 0; i < numberDigitsDecimals; i++) {
            decimal *= 10;
            denominator *= 10;
        }
        int numerator = (int) Math.round(decimal);
        int greatestCommonFactor = greatestCommonFactor(numerator, denominator);
        numerator1 = numerator / greatestCommonFactor;
        demonintor1 = denominator / greatestCommonFactor;
    }
    public int greatestCommonFactor(int num, int denom) {
        if (denom == 0) {
            return num;
        }
        return greatestCommonFactor(denom, num % denom); }
   private void decimalFromatConversion() {
        getEdittextData();
        String s = ed_odd.getText().toString().trim();
        if (s.contains("/")) {
            Double valD;
            String[] rat = s.split("/");
            String numnerator = rat[0];
            String denomintor = rat[1];
            valD = (Double.valueOf(numnerator) / Double.parseDouble(denomintor) + 1);
            edOddD = valD;
            ed_odd.setText("" + String.format("%.2f", valD));
        }

        if (s.startsWith("-")) {
            String a = s.replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            ed_odd.setText(String.format("%.2f", valD));   //-odd to decimal
            edOddD = valD;
        }

        if (ed_odd.getText().toString().startsWith("+")) {
            Double val = (Double.parseDouble(s) / 100) + 1;
            ed_odd.setText(String.valueOf(val));     //+odd to decimal
            edOddD = val;
        }
    }

}