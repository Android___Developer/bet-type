package com.example.bettype.fragmentcost;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.adapters.PlaceOneAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BigFragment extends Fragment {

    Context sContext;
    @BindView(R.id.reycler_one)
    RecyclerView reycler_one;
    @BindView(R.id.reycler_two)
    RecyclerView reycler_two;
    @BindView(R.id.reycler_three)
    RecyclerView reycler_three;
    @BindView(R.id.reycler_four)
    RecyclerView reycler_four;
    @BindView(R.id.reycler_five)
    RecyclerView reycler_five;
    @BindView(R.id.reycler_six)
    RecyclerView reycler_six;
    PlaceOneAdapter placeOneAdapter;
    List<String> list = new ArrayList<>();
    @BindView(R.id.lin_sencond)
    LinearLayout lin_sencond;
    @BindView(R.id.spinner_runners)
    Spinner spinner_runners;
    @BindView(R.id.lin_firstplace)
    LinearLayout lin_firstplace;
    ArrayAdapter adapter;
    String[] runner = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22", "23", "24"};

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_big, container, false);
        sContext = getActivity();
        ButterKnife.bind(this,view);
        Global.spinner(sContext, R.layout.spinner_item, runner, spinner_runners);
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
        recycler1();
        recycler2();
        recycler3();
        recycler4();
        recycler5();
        recycler6();
        return view;
    }

    private void recycler6() {
        reycler_six.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_six.setAdapter(placeOneAdapter);
        reycler_six.setNestedScrollingEnabled(false);
    }

    private void recycler5() {
        reycler_five.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_five.setAdapter(placeOneAdapter);
        reycler_five.setNestedScrollingEnabled(false);

    }

    private void recycler4() {
        reycler_four.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_four.setAdapter(placeOneAdapter);
        reycler_four.setNestedScrollingEnabled(false);
    }

    private void recycler3() {
        reycler_three.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_three.setAdapter(placeOneAdapter);
        reycler_three.setNestedScrollingEnabled(false);
    }

    private void recycler1() {
        reycler_one.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_one.setAdapter(placeOneAdapter);
        reycler_one.setNestedScrollingEnabled(false);
    }

    private void recycler2() {
        reycler_two.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_two.setAdapter(placeOneAdapter);
        reycler_two.setNestedScrollingEnabled(false);
    }
}


