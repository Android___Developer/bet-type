package com.example.bettype.fragmentcost;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.adapters.PlaceOneAdapter;
import com.example.bettype.adapters.PlaceTwoAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
public class QuinellaFragment extends Fragment {
    Context sContext;
    @BindView(R.id.reycler_one)
    RecyclerView reycler_one;
    @BindView(R.id.reycler_two)
    RecyclerView reycler_two;
    PlaceOneAdapter placeOneAdapter;
    PlaceTwoAdapter placeTwoAdapter;
    List<String> list=new ArrayList<>();
    List<String> list1=new ArrayList<>();
    @BindView(R.id.spinner_betoption)
    Spinner spinner_betoption;
    @BindView(R.id.spinner_runners)
    Spinner spinner_runners;
    String[] runner={"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16",
    "17","18","19","20","21","22","23","24"};
    String[] betoption={"Normal","Standout"};

   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quinella, container, false);
        sContext = getActivity();
        ButterKnife.bind(this,view);
       Global.spinner(sContext,R.layout.spinner_item,runner,spinner_runners);
       Global.spinner(sContext,R.layout.spinner_item,betoption,spinner_betoption);

       list.add("1");
       list.add("2");
       list.add("3");
       list.add("4");
       list.add("5");
       list.add("6");
       list.add("7");
       list.add("8");
       list.add("9");
       list.add("10");
       list.add("11");
       list.add("12");

        recycler1();
        recycler2();
        return view;
    }
    private void recycler1() {
       reycler_one.setLayoutManager(new GridLayoutManager(sContext,4));
       placeOneAdapter=new PlaceOneAdapter(sContext,list);
       reycler_one.setAdapter(placeOneAdapter);
       reycler_one.setNestedScrollingEnabled(false);

    }

    private void recycler2() {
        reycler_two.setLayoutManager(new GridLayoutManager(sContext,4));
        placeOneAdapter=new PlaceOneAdapter(sContext,list);
        reycler_two.setAdapter(placeOneAdapter);
        reycler_two.setNestedScrollingEnabled(false);
    }
}
