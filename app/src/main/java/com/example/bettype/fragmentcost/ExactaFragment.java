package com.example.bettype.fragmentcost;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.adapters.PlaceOneAdapter;
import com.example.bettype.adapters.PlaceTwoAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExactaFragment extends Fragment {
    Context sContext;
    @BindView(R.id.reycler_one)
    RecyclerView reycler_one;
    @BindView(R.id.reycler_two)
    RecyclerView reycler_two;
    PlaceOneAdapter placeOneAdapter;
    PlaceTwoAdapter placeTwoAdapter;
    List<String> list = new ArrayList<>();
    List<String> list1 = new ArrayList<>();
    @BindView(R.id.spinner_betoption)
    Spinner spinner_betoption;
    @BindView(R.id.lin_sencond)
    LinearLayout lin_sencond;
    @BindView(R.id.spinner_runners)
    Spinner spinner_runners;
    String[] runner = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16",
            "17", "18", "19", "20", "21", "22", "23", "24"};
    String[] betoption = {"Normal", "Standout", "Boxed", "Roving banker"};


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exacta, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        Global.spinner(sContext, R.layout.spinner_item, runner, spinner_runners);
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
betOptionSpinner();
        recycler1();
        recycler2();
        return view;
    }

    private void recycler1() {
        reycler_one.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_one.setAdapter(placeOneAdapter);
        reycler_one.setNestedScrollingEnabled(false);

    }

    private void recycler2() {
        reycler_two.setLayoutManager(new GridLayoutManager(sContext, 4));
        placeOneAdapter = new PlaceOneAdapter(sContext, list);
        reycler_two.setAdapter(placeOneAdapter);
        reycler_two.setNestedScrollingEnabled(false);
    }

    private void betOptionSpinner() {
        ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, betoption);
        spinner_betoption.setAdapter(heatAdapter);
        spinner_betoption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Boxed")) {
                    reycler_one.setVisibility(View.VISIBLE);
                    lin_sencond.setVisibility(View.GONE);
                } else {
                    reycler_one.setVisibility(View.VISIBLE);
                    lin_sencond.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}