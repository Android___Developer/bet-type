package com.example.bettype.model;

public class HorseModel {

    public String name;
    public int image;
    public int color_type;
    public HorseModel(String name, String s, String s1) {

    }

    public int getColor_type() {
        return color_type;
    }
    public void setColor_type(int color_type) {
        this.color_type = color_type;
    }

    public HorseModel(String name, int image, int color_type) {
        this.name = name;
        this.image = image;
        this.color_type = color_type;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }


}
