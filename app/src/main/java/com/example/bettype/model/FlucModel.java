package com.example.bettype.model;

public class FlucModel {
    int icon;

    public FlucModel(int icon) {
        this.icon = icon;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
