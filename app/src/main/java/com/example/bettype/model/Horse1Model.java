package com.example.bettype.model;

public class Horse1Model {
    public static final int HEADER = 0;
    public static final int ITEM = 1;

    public String name;
    public int image;
    public int color_type;
    public String header;
    int viewType;

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public Horse1Model(String name, int image, int color_type,int viewType) {
        this.name = name;
        this.image = image;
        this.color_type = color_type;
        this.viewType = viewType;
    }

    public String getName() {
        return name;
   }

    public void setName(String name) {
        this.name = name; }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getColor_type() {
        return color_type;
    }

    public void setColor_type(int color_type) {
        this.color_type = color_type;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }
}
