package com.example.bettype.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.bettype.R;

import com.example.bettype.fragments.Search;
import com.example.bettype.utilies.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtherCalculators extends AppCompatActivity {

    @BindView(R.id.setting)
    ImageView setting;
    @BindView(R.id.other_container)
    FrameLayout otherContainer;
    @BindView(R.id.search)
    ImageView search;
    @BindView(R.id.backbutton)
    ImageView backbutton;
    @BindView(R.id.searchright)
    ImageView searchright;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_calculators);
        Helper.setStatusBarColor(OtherCalculators.this, R.color.toolcolor);
        ButterKnife.bind(this);
    }
    @OnClick({R.id.search, R.id.backbutton, R.id.searchright,R.id.setting})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.search:
                break;
            case R.id.backbutton:
                onBackPressed();
                break;
            case R.id.searchright:
                getSupportFragmentManager().beginTransaction().replace(R.id.other_container,new Search()).commit();

                break;
            case R.id.setting:
                break;
        }
    }
}
