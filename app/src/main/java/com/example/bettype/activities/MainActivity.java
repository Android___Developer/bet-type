package com.example.bettype.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import com.example.bettype.R;
import com.example.bettype.adapters.TabsAdapter;
import com.example.bettype.fragments.BetCalculator;
import com.example.bettype.fragments.BetType;
import com.example.bettype.fragments.Bookies;
import com.example.bettype.fragments.Othercalculator;
import com.example.bettype.fragments.Search;
import com.example.bettype.utilies.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
   BottomNavigationView bottomNavigationView;
//   public  static ImageView search, searchright,setting,backbutton;
//   public static TextView lefttext,righttext;
   Context sContext;
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sContext=MainActivity.this;
        ButterKnife.bind(this);
        Helper.setStatusBarColor(MainActivity.this, R.color.toolcolor);
        loadFragment(new BetCalculator());
//        search=findViewById(R.id.search);
//        searchright=findViewById(R.id.searchright);
//        setting=findViewById(R.id.setting);
//        backbutton=findViewById(R.id.backbutton);
//        lefttext=findViewById(R.id.lefttext);
//        righttext=findViewById(R.id.righttext);
//        bottomNavigationView=findViewById(R.id.navigation_bottom);
//        bottomNavigationView.setOnNavigationItemSelectedListener(this);
//        getSupportFragmentManager().beginTransaction().replace(R.id.container,new BetCalculator()).commit();
//        search.setVisibility(View.VISIBLE);
//        searchright.setVisibility(View.GONE);
//        setting.setVisibility(View.VISIBLE);
//        backbutton.setVisibility(View.GONE);
//        backbutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               onBackPressed();
//            }
//        });
//        searchright.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getSupportFragmentManager().beginTransaction().replace(R.id.container,new Search()).commit();
//            }});
//        search.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getSupportFragmentManager().beginTransaction().replace(R.id.container,new Search()).commit();
//            }
//        });
   }
        @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.betcal:
                fragment=  new BetCalculator();
                break;

            case R.id.bettype:
                fragment = new BetType();
                break;

            case R.id.bookies:
                fragment = new Bookies();
                break;

            case R.id.othercal:
                fragment = new Othercalculator();
                break;
        }
        return loadFragment(fragment);
    }
    private boolean loadFragment(Fragment fragment) {

        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
            return true;
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if (bottomNavigationView.getSelectedItemId() == R.id.betcal) {
            finishAffinity();
        }
        else if(bottomNavigationView.getSelectedItemId() == R.id.bettype)
        {
            bottomNavigationView.setSelectedItemId(R.id.betcal);
        }
        else if(bottomNavigationView.getSelectedItemId() == R.id.bookies)
        {
            bottomNavigationView.setSelectedItemId(R.id.bettype);
        }
        else if(bottomNavigationView.getSelectedItemId() == R.id.othercal)
        {
            bottomNavigationView.setSelectedItemId(R.id.betcal);
        }
        else
        {
            super.onBackPressed();
        }
   }

}


