package com.example.bettype.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.fragments.BetCalculator;
import com.example.bettype.fragments.BetType;
import com.example.bettype.fragments.Bookies;
import com.example.bettype.fragments.Othercalculator;
import com.example.bettype.fragments.Search;
import com.example.bettype.utilies.Global;
import com.example.bettype.utilies.Helper;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity1 extends AppCompatActivity {
    Context sContext;
    BottomNavigationView bottomNavigationView;
    public static ImageView backbutton, setting, searchright, search;
    AlertDialog alertDialog;
    String[] deduction = {"No", "Yes"};
    String[] format = {"Decimal", "Fractional", "American"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        sContext = MainActivity1.this;
        ButterKnife.bind(this);
        viewId();
        Global.changeDashFragment(this, new BetCalculator());
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.betcal:
                        Global.changeDashFragment(sContext, new BetCalculator());
                        break;
                    case R.id.bettype:
                        Global.changeDashFragment(sContext, new BetType());
                        break;
                    case R.id.bookies:
                        Global.changeDashFragment(sContext, new Bookies());
                        break;
                    case R.id.othercal:
                        Global.changeDashFragment(sContext, new Othercalculator());
                        break;
                }
                return true;
            }
        });
    }

    private void viewId() {
        searchright = findViewById(R.id.searchright);
        setting = findViewById(R.id.setting);
        search = findViewById(R.id.search);
        backbutton = findViewById(R.id.backbutton);
        backbutton.setVisibility(View.GONE);
        search.setVisibility(View.VISIBLE);
        searchright.setVisibility(View.GONE);
        setting.setVisibility(View.VISIBLE);
        bottomNavigationView = findViewById(R.id.navigation_bottom);
        Helper.setStatusBarColor(MainActivity1.this, R.color.toolcolor);
    }

    @OnClick(R.id.search)
    public void search() {
        Global.changeDashFragment(sContext, new Search());
    }

    @OnClick(R.id.searchright)
    public void searchright() {
        Global.changeDashFragment(sContext, new Search());
    }

    @OnClick(R.id.backbutton)
    public void backbutton()
    {
       startActivity(new Intent(sContext, MainActivity1.class));
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_container);
        if (currentFragment instanceof BetCalculator) {
            //finishAffinity();
            Helper.exitButton(sContext); } else {
            super.onBackPressed();
        }
    }
}


