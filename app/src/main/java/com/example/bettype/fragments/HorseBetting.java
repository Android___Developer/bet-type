package com.example.bettype.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bettype.R;
import com.example.bettype.adapters.HorseBetAdapter1;
import com.example.bettype.exoticbetType.Big6ExoticFragment;
import com.example.bettype.exoticbetType.BoxtrifectaFragment;
import com.example.bettype.exoticbetType.DoubleBetExoticFragment;
import com.example.bettype.exoticbetType.DuetExoticFragmnet;
import com.example.bettype.exoticbetType.ExactaExoticFragment;
import com.example.bettype.exoticbetType.FirstFourExoticFragment;
import com.example.bettype.exoticbetType.FlexiExoticFragment;
import com.example.bettype.exoticbetType.QuadieExoticFragment;
import com.example.bettype.exoticbetType.QuniellaExoticFragment;
import com.example.bettype.exoticbetType.ToteExoticFragment;
import com.example.bettype.exoticbetType.TrebleExoticFragment;
import com.example.bettype.exoticbetType.TrifectaExoticFragment;
import com.example.bettype.model.Horse1Model;
import com.example.bettype.model.HorseModel;
import com.example.bettype.utilies.Global;
import com.example.bettype.winBetType.BestOfBestFragment;
import com.example.bettype.winBetType.BestToteFragment;
import com.example.bettype.winBetType.BettingWithoutFragment;
import com.example.bettype.winBetType.EachwayBetFragment;
import com.example.bettype.winBetType.FixedOddFragment;
import com.example.bettype.winBetType.MidtoteFragment;
import com.example.bettype.winBetType.ParlayWinBetFragment;
import com.example.bettype.winBetType.PlaceBettingFragment;
import com.example.bettype.winBetType.StartingpriceFragment;
import com.example.bettype.winBetType.ToteTopPlusFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.bettype.activities.MainActivity1.search;

public class HorseBetting extends Fragment {
    HorseBetAdapter1 horsebetAdapter;
    List<Horse1Model> list = new ArrayList<>();
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    TextView righttext, lefttext;
    @BindView(R.id.text_exotic)
    TextView text_exotic;
    Context sContext;
    @BindView(R.id.ed_search)
    SearchView ed_search;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_horse_betting, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        horsebetAdapter = new HorseBetAdapter1(getActivity(), list);
        setData();
        reycler1();
        search();
        return view;
    }

    private void search() {
        ed_search.setActivated(true);
        ed_search.setQueryHint("Search");
        ed_search.onActionViewExpanded();
        ed_search.clearFocus();
        ed_search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                horsebetAdapter.getFilter().filter(s);

               /* if (s.length()> 0) {
                   // reycler1();
                    horsebetAdapter.getFilter().filter(s);
                }
                else {
                    reycler1();
                }*/
                return false;
            }
        });
    }

    private void reycler1() {

        final GridLayoutManager manager = new GridLayoutManager(sContext, 4);
        recyclerview.setLayoutManager(manager);

        recyclerview.setAdapter(horsebetAdapter);

        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (position) {
                    case 11:
                        return 4;
                }
                return 1;
            }
        });

        horsebetAdapter.ItemCLick(new HorseBetAdapter1.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.changeDashFragment(sContext, new BestOfBestFragment());
                } else if (pos == 1) {
                    Global.changeDashFragment(sContext, new TopFlucFragment());
                }
                if (pos == 2) {
                    Global.changeDashFragment(sContext, new BestToteFragment());
                } else if (pos == 3) {
                    Global.changeDashFragment(sContext, new MidtoteFragment());
                }
                if (pos == 4) {
                    Global.changeDashFragment(sContext, new StartingpriceFragment());
                } else if (pos == 5) {
                    Global.changeDashFragment(sContext, new FixedOddFragment());
                }

                if (pos == 6) {
                    Global.changeDashFragment(sContext, new ToteTopPlusFragment());
                } else if (pos == 7) {
                    Global.changeDashFragment(sContext, new EachwayBetFragment());
                } else if (pos == 8) {
                    Global.changeDashFragment(sContext, new PlaceBettingFragment());
                } else if (pos == 9) {
                    Global.changeDashFragment(sContext, new BettingWithoutFragment());
                }

                if (pos == 10) {
                    Global.changeDashFragment(sContext, new ParlayWinBetFragment());
                }

                if (pos == 12) {
                    Global.changeDashFragment(sContext, new ToteExoticFragment());
                } else if (pos == 13) {
                    Global.changeDashFragment(sContext, new BoxtrifectaFragment());
                }
                if (pos == 14) {
                    Global.changeDashFragment(sContext, new FlexiExoticFragment());
                } else if (pos == 15) {
                    Global.changeDashFragment(sContext, new FirstFourExoticFragment());
                }
                if (pos == 16) {
                    Global.changeDashFragment(sContext, new ExactaExoticFragment());
                } else if (pos == 17) {
                    Global.changeDashFragment(sContext, new DoubleBetExoticFragment());
                }
                if (pos == 18) {
                    Global.changeDashFragment(sContext, new QuniellaExoticFragment());
                } else if (pos == 19) {
                    Global.changeDashFragment(sContext, new TrebleExoticFragment());
                }

                if (pos == 20) {
                    Global.changeDashFragment(sContext, new DuetExoticFragmnet());
                } else if (pos == 21) {

                    Global.changeDashFragment(sContext, new QuadieExoticFragment());
                }

                if (pos == 22) {
                    Global.changeDashFragment(sContext, new TrifectaExoticFragment());
                }

                if (pos == 23) {
                    Global.changeDashFragment(sContext, new Big6ExoticFragment());
                }
            }});
    }

    public void setData() {
        list.add(new Horse1Model("Best of the Best", R.drawable.horse, 1, Horse1Model.ITEM));
        list.add(new Horse1Model("Top Fluc", R.drawable.horse, 2, Horse1Model.ITEM));
        list.add(new Horse1Model("Best Tote", R.drawable.horse, 3, Horse1Model.ITEM));
        list.add(new Horse1Model("Mid Tote", R.drawable.horse, 4, Horse1Model.ITEM));
        list.add(new Horse1Model("Starting Price", R.drawable.horse, 1, Horse1Model.ITEM));
        list.add(new Horse1Model("Fixed odds", R.drawable.horse, 2, Horse1Model.ITEM));
        list.add(new Horse1Model("Top Tote Plus", R.drawable.horse, 3, Horse1Model.ITEM));
        list.add(new Horse1Model("Each Way Bet", R.drawable.horse, 4, Horse1Model.ITEM));
        list.add(new Horse1Model("Place betting", R.drawable.horse, 1, Horse1Model.ITEM));
        list.add(new Horse1Model("Betting Without", R.drawable.horse, 2, Horse1Model.ITEM));
        list.add(new Horse1Model("Parlay", R.drawable.horse, 3, Horse1Model.ITEM));

        list.add(new Horse1Model("Exotic bet type", 0, 3, Horse1Model.HEADER));

        list.add(new Horse1Model("Tote", R.drawable.horse, 1, Horse1Model.ITEM));
        list.add(new Horse1Model("Box trifecta", R.drawable.horse, 2, Horse1Model.ITEM));
        list.add(new Horse1Model("Flexi", R.drawable.horse, 3, Horse1Model.ITEM));
        list.add(new Horse1Model("First four", R.drawable.horse, 4, Horse1Model.ITEM));
        list.add(new Horse1Model("Exacta", R.drawable.horse, 1, Horse1Model.ITEM));
        list.add(new Horse1Model("Double bet", R.drawable.horse, 2, Horse1Model.ITEM));
        list.add(new Horse1Model("Quinella", R.drawable.horse, 3, Horse1Model.ITEM));
        list.add(new Horse1Model("Treble", R.drawable.horse, 4, Horse1Model.ITEM));
        list.add(new Horse1Model("Duet", R.drawable.horse, 1, Horse1Model.ITEM));
        list.add(new Horse1Model("Quaddie", R.drawable.horse, 2, Horse1Model.ITEM));
        list.add(new Horse1Model("Trifecta", R.drawable.horse, 3, Horse1Model.ITEM));
        list.add(new Horse1Model("Big 6", R.drawable.horse, 4, Horse1Model.ITEM));


    }

}






