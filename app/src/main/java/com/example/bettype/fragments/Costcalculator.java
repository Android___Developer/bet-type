package com.example.bettype.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.activities.MainActivity;
import com.example.bettype.fragmentcost.BigFragment;
import com.example.bettype.fragmentcost.DailyFragment;
import com.example.bettype.fragmentcost.DuetFragment;
import com.example.bettype.fragmentcost.ExactaFragment;
import com.example.bettype.fragmentcost.FirstFourFragment;
import com.example.bettype.fragmentcost.QuaddieFragment;
import com.example.bettype.fragmentcost.QuinellaFragment;
import com.example.bettype.fragmentcost.TrifectaFragment;
import com.example.bettype.fragmentodd.AccumultorFragmnet;
import com.example.bettype.fragmentodd.CandianFragmnet;
import com.example.bettype.fragmentodd.DoubleFragment;
import com.example.bettype.fragmentodd.EachwayFragment;
import com.example.bettype.fragmentodd.GoliathFragmnet;
import com.example.bettype.fragmentodd.HenizFragment;
import com.example.bettype.fragmentodd.LuckyFragment;
import com.example.bettype.fragmentodd.LuckySixFragment;
import com.example.bettype.fragmentodd.LuckythreeFragmnet;
import com.example.bettype.fragmentodd.MultibetFragmnet;
import com.example.bettype.fragmentodd.ParlayFragment;
import com.example.bettype.fragmentodd.PatentFragmnet;
import com.example.bettype.fragmentodd.SingleFragment;
import com.example.bettype.fragmentodd.SuperHenizFragmnet;
import com.example.bettype.fragmentodd.SuperYankFragmnet;
import com.example.bettype.fragmentodd.TrebleFragment;
import com.example.bettype.fragmentodd.TrexiFragment;
import com.example.bettype.fragmentodd.YankeeFragment;
import com.example.bettype.utilies.Global;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;


public class Costcalculator extends Fragment {
    Context sContext;
    @BindView(R.id.spinner_bettype)
    Spinner spinner_bettype;
    String[] bettype = {"Please select calcultor", "Quinella", "Exacta", "Duet", "Trifecta", "First Four",
    "Daily Double","Quaddie","Big 6"};
    String[] outcome = {"Win", "Loss", "Placed","Dead Heat", "Void"};
    String[] heat = {"First", "Second", "Third",};
    String[] paid = {"One", "Two", "Three",};
    String[] runner = {"Two", "Three", "Four",};
    String [] returnn={"Win","Dead heat","Loss","Placed","Void"};
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_costcalculator, container, false);
        sContext=getActivity();
        ButterKnife.bind(this,view);
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        spinnerbettype();
        return view;
    }
    private void spinnerbettype() {
        ArrayAdapter adapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, bettype);
        spinner_bettype.setAdapter(adapter);
        spinner_bettype.setSelection(1);
        spinner_bettype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Quinella")) {
                    Global.singleFragment(sContext, new QuinellaFragment());
                } else if (text.equals("Exacta")) {
                    Global.singleFragment(sContext, new ExactaFragment());
                } else if (text.equals("Duet")) {
                    Global.singleFragment(sContext, new DuetFragment());
                } else if (text.equals("Trifecta")) {
                    Global.singleFragment(sContext, new TrifectaFragment());
                } else if (text.equals("First Four")) {
                    Global.singleFragment(sContext, new FirstFourFragment());
                } else if (text.equals("Daily Double")) {
                    Global.singleFragment(sContext, new DailyFragment());
                } else if (text.equals("Quaddie")) {
                    Global.singleFragment(sContext, new QuaddieFragment());
                } else if (text.equals("Big 6")) {
                    Global.singleFragment(sContext, new BigFragment());
                } else {
                } }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

}

