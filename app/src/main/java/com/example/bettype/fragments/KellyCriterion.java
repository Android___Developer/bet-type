package com.example.bettype.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.bettype.R;
import com.example.bettype.utilies.Helper;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;
import static java.lang.String.valueOf;

public class KellyCriterion extends Fragment implements View.OnClickListener {
    private EditText edBetBank, edMultipler, edOdd, edWinning;
    private Button btnBetstake, btnBank, btncal;
    private Context sContext;
    private Double betBankD, multiplerD, oddD, winningD;
    private ImageView imgSetting;
    private AlertDialog alertDialog;
    String[] fomrat = {"Decimal", "Fractional", "American"};
    private LinearLayout linearBackgroundClick;
    private int numerator1, demonintor1;
    private String fractionResult = "";
    private int denominator = 1;
    int res;
    Spinner spinner_format;
    ArrayAdapter adapter;
    String number;
    String item = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_kelly_criterion, container, false);
        sContext = getActivity();
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        Helper.clearPref(sContext);
        init(view);

        keyboradDoneListener();
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    private void init(View view) {
        edBetBank = view.findViewById(R.id.edBetBank);
        edOdd = view.findViewById(R.id.edOdd);
        edMultipler = view.findViewById(R.id.edMultipler);
        edWinning = view.findViewById(R.id.edWinning);
        imgSetting = view.findViewById(R.id.imgSetting);
        btnBetstake = view.findViewById(R.id.btnBetstake);
        btnBank = view.findViewById(R.id.btnBank);
        btncal = view.findViewById(R.id.btncal);
        linearBackgroundClick = view.findViewById(R.id.linearBackgroundClick);
        btncal.setOnClickListener(this);
        btnBank.setOnClickListener(this);
        btnBetstake.setOnClickListener(this);
        imgSetting.setOnClickListener(this);
        linearBackgroundClick.setOnClickListener(this);
        edMultipler.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                } else if (Double.parseDouble(s.toString()) > 100) {
                    s.replace(0, s.length(), "100");
                }
            }
        });
    }

    private void keyboradDoneListener() {
        edBetBank.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edBetBank.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            betBankD = Double.parseDouble(number);
                            checkCondition();
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edOdd.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            oddD = Double.parseDouble(number);
                            checkCondition();
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edMultipler.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edMultipler.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            multiplerD = Double.parseDouble(number);
                            checkCondition();
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edWinning.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edWinning.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            winningD = Double.parseDouble(number);
                            checkCondition();
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void decimalFormat() {

        String s = edOdd.getText().toString().trim();

        if (s.contains("/")) {
            Double valD;
            String[] rat = s.split("/");
            String numnerator = rat[0];
            String denomintor = rat[1];
            valD = (Double.valueOf(numnerator) / Double.parseDouble(denomintor) + 1);
            oddD = valD;
            edOdd.setText("" + String.format("%.2f", valD));}

        if (s.startsWith("-")) {
            String a =s.replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            edOdd.setText(String.format("%.2f", valD));   //-odd to decimal
            oddD = valD;
        }

        if (edOdd.getText().toString().startsWith("+")) {
            Double val = (Double.parseDouble(s) / 100) + 1;
            edOdd.setText(String.valueOf(val));     //+odd to decimal
        }

        decimalConversion(oddD);
    }

    private void decimalConversion(Double d) {
        getAllValue();
        String a = Double.toString(d - 1);
        Double temp1 = Double.parseDouble(a) * winningD / 100;
        String b = valueOf((1 - winningD / 100));
        Double temp2 = Double.parseDouble(b);
        String c = valueOf((d - 1));
        Double temp3 = Double.parseDouble(c);
        String cal = valueOf(((temp1 - temp2) / temp3));
        Double calD = Double.valueOf(cal);
        String percent = valueOf((calD * (multiplerD / 100)));
        Double percentD = Double.parseDouble(percent);
        String finaal = valueOf(((percentD) * 100));
        Double finalD = Double.parseDouble(finaal);
        Double finBet = (finalD * betBankD) / 100;

        if (finalD < 0) {
            btnBetstake.setText("Don't Bet");
        } else {
            btnBetstake.setText(String.format("%.2f", finBet));
        }
        btnBank.setText(String.format("%.2f", finalD) + "%");
    }
    private void getAllValue() {

        betBankD = Double.parseDouble(edBetBank.getText().toString().trim());
        multiplerD = Double.parseDouble(edMultipler.getText().toString().trim());
        oddD = Double.parseDouble(edOdd.getText().toString().trim());
        winningD = Double.parseDouble(edWinning.getText().toString().trim());

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgSetting:
                getAlertDailog();
                break;
            case R.id.linearBackgroundClick:
                try {
                    hideKeyboard((Activity) sContext);
                    /// checkCondition();
                } catch (Exception e) {
                }
                break;
        }
    }

    private void getAlertDailog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sContext);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.alert_kelly, null);
        dialogBuilder.setView(dialogView);
        spinner_format = dialogView.findViewById(R.id.spinner_format);
        TextView txt_setting = dialogView.findViewById(R.id.txt_setting);
        alertSpinner();
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        txt_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.setString(sContext, "pos", String.valueOf(item));
                spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "pos")), true);
                checkCondition();
                alertDialog.dismiss();
            }
        });
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
    }
    private void alertSpinner() {
        adapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, fomrat);
        spinner_format.setAdapter(adapter);
        if (Helper.getString(sContext, "pos").equals("")) {
            Helper.setString(sContext, "pos", "0");
        } else {
            spinner_format.setSelection(Integer.parseInt(Helper.getString(sContext, "pos")), true);
        }
        spinner_format.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = String.valueOf(parent.getItemIdAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
    private void checkCondition() {

        if (item.equals("0")) {
            try {
                decimalFormat();
            } catch (Exception e) {
            }
        } else if (item.equals("1")) {
            try {

                fractionalFormat();
            } catch (Exception e) {
            }
        } else if (item.equals("2")) {
            try {
                americanFormat();
            } catch (Exception e) {
            }

        }
        if (Helper.getString(sContext, "pos").equals("")) {
            decimalFormat();
        }
    }
    private void hideKeyboard(Activity context) {
        View v = context.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public void Fraction(double decimal) {
        String stringNumber = String.valueOf(decimal);
        int numberDigitsDecimals = stringNumber.length() - 1 - stringNumber.indexOf('.');
        denominator = 1;
        for (int i = 0; i < numberDigitsDecimals; i++) {
            decimal *= 10;
            denominator *= 10;
        }
        int numerator = (int) Math.round(decimal);
        int greatestCommonFactor = greatestCommonFactor(numerator, denominator);
        numerator1 = numerator / greatestCommonFactor;
        demonintor1 = denominator / greatestCommonFactor;
    }
    public int greatestCommonFactor(int num, int denom) {
        if (denom == 0) {
            return num;
        }
        return greatestCommonFactor(denom, num % denom);
    }

    private void fractionalFormat() {
        String s = edOdd.getText().toString();
        if (edOdd.getText().toString().contains("-"))
        {
            String a = edOdd.getText().toString().replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            Fraction(Double.parseDouble(String.format("%.2f", valD)));
            int res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            edOdd.setText("" + fractionResult);
        }
        else  if (s.contains("+"))
        {
            Double val1 = (Double.parseDouble(s) / 100);
            Fraction(val1);
            String fResult = (numerator1) + "/" + demonintor1;
            edOdd.setText("" + fResult);              //+oddd to fractionl

        }
        else {
            Fraction(Double.parseDouble(String.format("%.2f", oddD)));
            res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            edOdd.setText(fractionResult);
        }


        edOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String ss = edOdd.getText().toString();
                    hideKeyboard((Activity) sContext);
                    if (ss.length() > 0) {
                        try {
                            if (ss.contains("/")) {
                                String[] rat = ss.split("/");
                                String n = rat[0];
                                String vv = rat[1];
                                Double valD = Double.valueOf((Double.parseDouble(n) / Double.parseDouble(vv)) + 1);
                                newDecimalFormat(valD);
                            }
                        } catch (Exception e) {

                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    private void newDecimalFormat(Double numb) {
        String a = Double.toString(numb - 1);
        Double temp1 = Double.parseDouble(a) * winningD / 100;
        String b = valueOf((1 - winningD / 100));
        Double temp2 = Double.parseDouble(b);
        String c = valueOf((numb - 1));
        Double temp3 = Double.parseDouble(c);
        String cal = valueOf(((temp1 - temp2) / temp3));
        Double calD = Double.valueOf(cal);
        String percent = valueOf((calD * (multiplerD / 100)));
        Double percentD = Double.parseDouble(percent);
        String finaal = valueOf(((percentD) * 100));
        Double finalD = Double.parseDouble(finaal);
        Double finBet = (finalD * betBankD) / 100;

        if (finalD < 0) {
            btnBetstake.setText("Don't Bet");
        } else {

            btnBetstake.setText(String.format("%.2f", finBet));
        }

        btnBank.setText(String.format("%.2f", finalD) + "%");
    }
    private void americanFormat() {
        // getAllValue();
        String ss = edOdd.getText().toString();
        if (ss.contains("/")) {
            String[] rat = ss.split("/");
            String n = rat[0];
            String v = rat[1];

            if (Double.valueOf(Double.parseDouble(n)) > Double.valueOf(Double.parseDouble(v))) {
                Double valM = (Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v)) * 100));
                Double a = Double.parseDouble(String.valueOf(valM));
                int i = a.intValue();
                americanConversin(Double.valueOf(i));
                edOdd.setText("+" + "" + i);
            } else if (Double.valueOf(Double.parseDouble(n)) < Double.valueOf(Double.parseDouble(v))) {
                Double valm = ((100) / Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v))));
                Double aa = Double.parseDouble(String.valueOf(valm));
                int j = aa.intValue();
                americanConversin(Double.valueOf(j));
                edOdd.setText("-" + "" + j);
            }
        }
        else
        { if (oddD >= 2) {
            Double valPostive = (oddD - 1) * 100;
            int i = Integer.valueOf(valPostive.intValue());
            edOdd.setText("+" + "" + i);
            americanConversin(Double.valueOf(i));

        } else {
            Double valNegative = (-100) / (oddD - 1);
            int j = valNegative.intValue();
            edOdd.setText("" + j);
        }
        }

//        if (ss.startsWith("+")) {
//            String b = ss.replace("+", "");
//            Double val = (Double.parseDouble(b) / 100) + 1;
//            americanConversin(val);}
//
//        if (ss.startsWith("-")) {
//            String a =ss.replace("-", "");
//            Double vaD = ((100 / Double.parseDouble(a)) + 1);
//            americanConversin(vaD);
//        }


        edOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String ss = edOdd.getText().toString();
                    if (ss.length() > 0) {
                        try {
                            if (ss.startsWith("-")) {
                                String a = ss.replace("-", "");
                                Double valD = ((100 / Double.parseDouble(a)) + 1);
                                americanConversin(valD);

                            } else if (ss.startsWith("+")) {
                                String b = ss.replace("+", "");
                                Double val = (Double.parseDouble(b) / 100) + 1;
                                americanConversin(val);
                            }
                        } catch (Exception e) {
                            return true;
                        }
                    }
                }
                return false;
            }
        });
    }

    private void americanConversin(Double numb) {
        String ss = edOdd.getText().toString();
        if (ss.startsWith("-")) {
            String a = ss.replace("-", "");
            Double valD = ((100 / Double.parseDouble(a)) + 1);
            numb = Double.valueOf(valD);
            decimalConversion(numb);
        }
        if (ss.startsWith("+")) {
            String b = ss.replace("+", "");
            Double val = (Double.parseDouble(b) / 100) + 1;
            numb = Double.valueOf(val);
            decimalConversion(numb);
        }


    }
}