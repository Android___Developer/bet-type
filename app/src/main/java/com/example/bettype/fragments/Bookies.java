package com.example.bettype.fragments;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.bettype.R;
import com.example.bettype.activities.MainActivity;
import com.example.bettype.adapters.BookeiesAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;

public class Bookies extends Fragment {
    Context sContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_bookies, container, false);
        sContext=getActivity();
        ButterKnife.bind(this,view);
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        return view;
    }
    @OnClick(R.id.rel_sportbet)
    public void rel_sportbet()
    {
        Global.urlLink(sContext,"https://promotions.sportsbet.com.au/racing-501-adss/?dclid=CjkKEQjwyqTqBRDGj-HSubys9bwBEiQAnb2ye6OuXRe9mJ9vD-r1xmvLdJ5gPzhnZmg9HFU4U-R37Grw_wcB");
    }

    @OnClick(R.id.rel_betsay)
    public void rel_betsay()
    {
        Global.urlLink(sContext,"https://beteasy.com.au/join-now?");
    }

    @OnClick(R.id.rel_ned)
    public void rel_ned()
    {
        Global.urlLink(sContext,"https://www.neds.com.au/");
    }

    @OnClick(R.id.rel_pointset)
    public void rel_pointset()
    {
        Global.urlLink(sContext,"https://pointsbet.com.au/signup?promo=DF1000&rf=df&md=Website&cp=DigitalFuel%20Signup");
    }

    @OnClick(R.id.rel_betthree)
    public void rel_betthree()
    {
        Global.urlLink(sContext,"https://www.bet365.com/en/?forcelp=1&affiliate=365_381347");
    }

    @OnClick(R.id.rel_labrook)
    public void rel_labrook()
    {
        Global.urlLink(sContext,"http://www.ladbrokes.com.au/signup/?a=510766&d=FBR500&c=2"); }

    @OnClick(R.id.rel_unibet)
    public void rel_unibet()
    {
        Global.urlLink(sContext,"https://www.unibet.com.au/promotions?mktid=1:81748421:911995-30888");
    }

    @OnClick(R.id.palmerbet)
    public void palmerbet()
    {
        Global.urlLink(sContext,"https://www.palmerbet.com/createaccount?aff=SAq1Cyjs6sRPmRZ9nB30WmNd7ZgqdRLk");
    }

    @OnClick(R.id.rel_betstar)
    public void rel_betstar()
    {
        Global.urlLink(sContext,"https://www.betstar.com.au/?signup=1&token=w5o8EVanAPNOHW2t-7g7HWNd7ZgqdRLk&b=FBR250&d=&a=1071&c=1&gclid=");
    }

    @OnClick(R.id.madbookie)
    public void madbookie()
    {
        Global.urlLink(sContext,"https://www.madbookie.com.au/account/register?Referrer=MBXUk8FfYtuMeWCX0XSb9GNd7ZgqdRLk&");
    }


}
