package com.example.bettype.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.adapters.HorsebetAdapter;
import com.example.bettype.model.HorseModel;
import com.example.bettype.sportsBet.AsainFragment;
import com.example.bettype.sportsBet.CorrectScoreFragment;
import com.example.bettype.sportsBet.DrawNoFragment;
import com.example.bettype.sportsBet.FirstGoalscoresFragment;
import com.example.bettype.sportsBet.FootyBettingFragment;
import com.example.bettype.sportsBet.HafFullTimeFragment;
import com.example.bettype.sportsBet.HandicapFragment;
import com.example.bettype.sportsBet.HeadToHeadFragment;
import com.example.bettype.sportsBet.LayBettingFragment;
import com.example.bettype.sportsBet.LineFragment;
import com.example.bettype.sportsBet.LiveBettingFragment;
import com.example.bettype.sportsBet.MarginFragment;
import com.example.bettype.sportsBet.SportMultiBetFragment;
import com.example.bettype.sportsBet.SameGameMultiFragment;
import com.example.bettype.sportsBet.TribetFragment;
import com.example.bettype.sportsBet.WinDrawFragment;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SportsBetting extends Fragment {
    List<HorseModel> list = new ArrayList<>();
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    HorsebetAdapter sportBetAdapter;
    Context sContext;
    @BindView(R.id.ed_search)
    SearchView ed_Search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sports_betting, container, false);
        ButterKnife.bind(this, view);
        sContext = getActivity();
        setData();
        recycler();
        search();
        return view;
    }

    private void search() {
        ed_Search.setActivated(true);
        ed_Search.setQueryHint("Search");
        ed_Search.onActionViewExpanded();
        ed_Search.setIconified(false);
        ed_Search.clearFocus();
       ed_Search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
          @Override
          public boolean onQueryTextSubmit(String s) {
              if(list.size()>0)
              {
                  recycler();
              }

             sportBetAdapter.getFilter().filter(s);
              return false;
          }

          @Override
          public boolean onQueryTextChange(String s) {
              if(list.size()>0)
              {
                  recycler();
              }
              sportBetAdapter.getFilter().filter(s);
              return false;
          }
      });
    }

    private void recycler() {
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 4));
        sportBetAdapter = new HorsebetAdapter(getActivity(), list);
        recyclerView.setAdapter(sportBetAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        sportBetAdapter.onItemClickListner(new HorsebetAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.changeDashFragment(sContext, new HeadToHeadFragment());
                } else if (pos == 1) {

                    Global.changeDashFragment(sContext, new WinDrawFragment());
                } else if (pos == 2) {
                    Global.changeDashFragment(sContext, new LayBettingFragment());
                } else if (pos == 3) {
                    Global.changeDashFragment(sContext, new LiveBettingFragment());
                } else if (pos == 4) {
                    Global.changeDashFragment(sContext, new HandicapFragment());
                } else if (pos == 5) {
                    Global.changeDashFragment(sContext, new AsainFragment());
                } else if (pos == 6) {
                    Global.changeDashFragment(sContext, new DrawNoFragment());
                } else if (pos == 7) {
                    Global.changeDashFragment(sContext, new CorrectScoreFragment());
                } else if (pos == 8) {
                    Global.changeDashFragment(sContext, new HafFullTimeFragment());
                } else if (pos == 9) {
                    Global.changeDashFragment(sContext, new MarginFragment());
                } else if (pos == 10) {
                    Global.changeDashFragment(sContext, new FootyBettingFragment());
                } else if (pos == 11) {
                    Global.changeDashFragment(sContext, new TribetFragment());
                } else if (pos == 12) {
                    Global.changeDashFragment(sContext, new SameGameMultiFragment());
                } else if (pos == 13) {
                    Global.changeDashFragment(sContext, new SportMultiBetFragment());
                } else if (pos == 14) {
                    Global.changeDashFragment(sContext, new FirstGoalscoresFragment());
                } else if (pos == 15) {
                    Global.changeDashFragment(sContext, new LineFragment());
                }
            }
        });
    }

    public void setData() {
        list.add(new HorseModel("Head to Head", R.drawable.sporticon, 1));
        list.add(new HorseModel("Win Draw Win", R.drawable.sporticon, 2));
        list.add(new HorseModel("Lay betting", R.drawable.sporticon, 3));
        list.add(new HorseModel("Live betting", R.drawable.sporticon, 4));
        list.add(new HorseModel("Handicap betting", R.drawable.sporticon, 1));
        list.add(new HorseModel("Asian Handicap", R.drawable.sporticon, 2));
        list.add(new HorseModel("Draw No Bet", R.drawable.sporticon, 3));
        list.add(new HorseModel("Correct score", R.drawable.sporticon, 4));
        list.add(new HorseModel("Half time full time", R.drawable.sporticon, 1));
        list.add(new HorseModel("Margin betting", R.drawable.sporticon, 2));
        list.add(new HorseModel("Footy betting", R.drawable.sporticon, 3));
        list.add(new HorseModel("Tri bet", R.drawable.sporticon, 4));
        list.add(new HorseModel("Same game multi", R.drawable.sporticon, 1));
        list.add(new HorseModel("Multi bet", R.drawable.sporticon, 2));
        list.add(new HorseModel("First goalscorer", R.drawable.sporticon, 3));
        list.add(new HorseModel("Line betting", R.drawable.sporticon, 4));
    }



}
