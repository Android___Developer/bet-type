package com.example.bettype.fragments;

import android.app.Activity;
import android.content.Context;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.bettype.R;
import com.example.bettype.activities.MainActivity;
import com.example.bettype.fragmentodd.AccumultorFragmnet;
import com.example.bettype.fragmentodd.CandianFragmnet;
import com.example.bettype.fragmentodd.DoubleFragment;
import com.example.bettype.fragmentodd.EachwayFragment;
import com.example.bettype.fragmentodd.GoliathFragmnet;
import com.example.bettype.fragmentodd.HenizFragment;
import com.example.bettype.fragmentodd.LuckyFragment;
import com.example.bettype.fragmentodd.LuckySixFragment;
import com.example.bettype.fragmentodd.LuckythreeFragmnet;
import com.example.bettype.fragmentodd.MultibetFragmnet;
import com.example.bettype.fragmentodd.ParlayFragment;
import com.example.bettype.fragmentodd.PatentFragmnet;
import com.example.bettype.fragmentodd.SingleFragment;
import com.example.bettype.fragmentodd.SuperHenizFragmnet;
import com.example.bettype.fragmentodd.SuperYankFragmnet;
import com.example.bettype.fragmentodd.TrebleFragment;
import com.example.bettype.fragmentodd.TrexiFragment;
import com.example.bettype.fragmentodd.YankeeFragment;
import com.example.bettype.utilies.Global;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;


public class Oddcalculator extends Fragment {
    Context sContext;
    @BindView(R.id.spinner_bettype)
    Spinner spinner_bettype;
    ArrayAdapter adapter;
    @BindView(R.id.spinner_outcome)
    Spinner spinner_outcome;
    @BindView(R.id.lin_spinner_item)
    LinearLayout lin_spinner_item;
    @BindView(R.id.spin_dead)
    Spinner spin_dead;
    @BindView(R.id.spin_paid)
    Spinner spin_paid;
    @BindView(R.id.spin_runner)
    Spinner spin_runner;
    String[] bettype = {"Select calculator", "Single", "Each way", "Double", "Treble", "Multi bet",
            "Accumulator", "Parlay", "Trixie", "Patent", "Yankee", "Lucky 15", "Canadian",
            "Lucky 31", "Heinz", "Lucky 63","Super Heinz", "Super Yankee","Goliath"};
    String[] outcome = {"Win", "Loss", "Placed","Dead Heat", "Void"};
    String[] heat = {"First", "Second", "Third",};
    String[] paid = {"One", "Two", "Three",};
    String[] runner = {"Two", "Three", "Four",};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_oddcalculator, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        setting.setVisibility(View.VISIBLE);
        searchright.setVisibility(View.GONE);
        spinnerbettype();
        spinnerDeadHeat();
        Global.spinner(sContext, R.layout.spinner_item, heat, spin_dead);
        Global.spinner(sContext, R.layout.spinner_item, paid, spin_paid);
        Global.spinner(sContext, R.layout.spinner_item, runner, spin_runner);
        return view;
    }

    private void spinnerbettype() {
        ArrayAdapter adapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, bettype);
        spinner_bettype.setAdapter(adapter);
        spinner_bettype.setSelection(1);
        spinner_bettype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Single")) {
                    Global.singleFragment(sContext, new SingleFragment()); }
               else if (text.equals("Each way")) {
                    Global.singleFragment(sContext, new EachwayFragment());
                }
                else  if (text.equals("Double")) {
                    Global.singleFragment(sContext, new DoubleFragment());
                }
                else   if (text.equals("Treble")) {
                    Global.singleFragment(sContext, new TrebleFragment());
                }
                else    if (text.equals("Multi bet")) {
                    Global.singleFragment(sContext, new MultibetFragmnet());
                }
                else  if (text.equals("Accumulator")) {

                    Global.singleFragment(sContext, new AccumultorFragmnet());
                }
                else  if (text.equals("Parlay")) {
                    Global.singleFragment(sContext, new ParlayFragment());

                } else if (text.equals("Trixie")) {
                    Global.singleFragment(sContext, new TrexiFragment());
                } else if (text.equals("Patent")) {
                    Global.singleFragment(sContext, new PatentFragmnet());
                }
                else if (text.equals("Yankee")) {
                    Global.singleFragment(sContext, new YankeeFragment());
                }
                else if (text.equals("Lucky 15")) {
                    Global.singleFragment(sContext, new LuckyFragment());
                }
                else   if (text.equals("Canadian")) {
                    Global.singleFragment(sContext, new CandianFragmnet());
                }
                else if (text.equals("Goliath")) {
                    Global.singleFragment(sContext, new GoliathFragmnet());
                }
                else if (text.equals("Super Heinz")) {
                    Global.singleFragment(sContext, new SuperHenizFragmnet());
                }
                else if (text.equals("Lucky 63")) {
                    Global.singleFragment(sContext, new LuckySixFragment());
                }
                else if (text.equals("Heinz")) {
                    Global.singleFragment(sContext, new HenizFragment());
                }
                else if (text.equals("Super Yankee")) {
                    Global.singleFragment(sContext, new SuperYankFragmnet());
                }
                else if (text.endsWith("Lucky 31"))
                {Global.singleFragment(sContext, new LuckythreeFragmnet());
                } else {

                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }
        private void spinnerDeadHeat() {
        ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, outcome);
        spinner_outcome.setAdapter(heatAdapter);
        spinner_outcome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();
                if (text.equals("Dead Heat")) {
                    lin_spinner_item.setVisibility(View.VISIBLE);
                } else {
                    lin_spinner_item.setVisibility(View.GONE); } }
                    @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        }); }

}
