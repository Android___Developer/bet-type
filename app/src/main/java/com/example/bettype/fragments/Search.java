package com.example.bettype.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.example.bettype.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class Search extends Fragment {
    Context sContext;
    @BindView(R.id.lin_match_betting)
    LinearLayout lin_match_betting;
    Unbinder unbinder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_search, container, false);
        sContext=getActivity();
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.lin_betcalculator)
    public void lin_betcalculator()
    {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new BetCalculator()).commit();
    }

    @OnClick(R.id.lin_bettype)
    public void lin_bettype()
    {
      //  getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container,new BetType()).commit();
    }

    @OnClick(R.id.lin_bookie)
    public void lin_bookie()
    {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new Bookies()).commit();
    }

    @OnClick(R.id.lin_avrage_calculator)
    public void lin_avrage_calculator()
    {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new ArbitrageCalculator()).commit();
    }


    @OnClick(R.id.lin_dutching)
    public void lin_dutching()
    {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new Dutchingcalculator()).commit();
    }


    @OnClick(R.id.lin_match_betting)
    public void lin_match_betting()
    {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new MatchedBetCalculator()).commit();
    }


    @OnClick(R.id.lin_kely)
    public void lin_kely()
    {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new KellyCriterion()).commit();
    }

    @OnClick(R.id.lin_oddcalulator)
    public void lin_oddcalulator()
   {
//        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container
//                ,new OddsConverter()).commit();
    }
}
