package com.example.bettype.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bettype.R;
import com.example.bettype.activities.MainActivity;


public class Mainfragment extends Fragment {

    public static TextView lefttab,righttab;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.fragment_mainfragment, container, false);
        lefttab=view.findViewById(R.id.lefttext);
        righttab=view.findViewById(R.id.righttext);
        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new Oddcalculator()).commit();


//        MainActivity.righttext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new Costcalculator()).commit();
//
//            }
//        });
//        MainActivity.lefttext.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,new Oddcalculator()).commit();
//
//            }
//        });



        return view;
    }

   }
