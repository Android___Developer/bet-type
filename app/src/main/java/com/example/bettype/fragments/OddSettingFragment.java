package com.example.bettype.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bettype.R;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;

public class OddSettingFragment extends Fragment {
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_oddsetting, container, false);
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        return view;
    }
}
