package com.example.bettype.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.bettype.R;
import com.example.bettype.utilies.Global;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;
public class BetCalculator extends Fragment {
    @BindView(R.id.left_arrowicon)
    ImageView left_arrowicon;
    @BindView(R.id.right_arrowicon)
    ImageView right_arrowicon;
    Context sContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_bet_calculator, container, false);
        sContext=getActivity();
        ButterKnife.bind(this,view);
        backbutton.setVisibility(View.GONE);
        search.setVisibility(View.VISIBLE);
        searchright.setVisibility(View.GONE);
        setting.setVisibility(View.VISIBLE);
        Global.betCalculator(getActivity(),new Oddcalculator());
        return view;
    }

    @OnClick(R.id.lefttext)
    public void lefttext()
    {
        left_arrowicon.setVisibility(View.VISIBLE);
        right_arrowicon.setVisibility(View.GONE);
       Global.betCalculator(sContext,new Oddcalculator());
    }

    @OnClick(R.id.righttext)
    public void righttext()
    {   left_arrowicon.setVisibility(View.GONE);
        right_arrowicon.setVisibility(View.VISIBLE);
        Global.betCalculator(sContext,new Costcalculator());
    }
}
