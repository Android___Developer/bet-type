package com.example.bettype.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.utilies.Helper;

import java.text.DecimalFormat;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;

public class OddsConverter extends Fragment {
    private final String TAG = getClass().getName();
    private Context sContext;
    private EditText edDecimal;
    private EditText edMoneyLine;
    private EditText edFractional;
    private TextView txtProbality;
    private Double decimalD, fractionalD, moneyLineD;
    private int numerator1, demonintor1;
    private String fractionResult = "";
    private int res;
    private int denominator = 1;
    private LinearLayout linearBackgroundClick;
    String number;
    int i, j;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_odds_converter, container, false);
        sContext = getActivity();
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        init(view);
        keybordDoneListener();
        return view;
    }

    private void init(View view) {
        edDecimal = view.findViewById(R.id.edDecimal);
        edFractional = view.findViewById(R.id.edFractional);
        edMoneyLine = view.findViewById(R.id.edMoneyLine);
        txtProbality = view.findViewById(R.id.txtProbality);
        linearBackgroundClick = view.findViewById(R.id.linearBackgroundClick);

    }

    private void keybordDoneListener() {
        edFractional.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edFractional.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            fractionToAll(number);
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edDecimal.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edDecimal.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            convertDecimaltoAll(number);
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });


        edMoneyLine.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    number = edMoneyLine.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            convertMoneyLineToall(number);
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }

    public void Fraction(double decimal) {
        String stringNumber = String.valueOf(decimal);
        int numberDigitsDecimals = stringNumber.length() - 1 - stringNumber.indexOf('.');
        denominator = 1;
        for (int i = 0; i < numberDigitsDecimals; i++) {
            decimal *= 10;
            denominator *= 10;
        }
        int numerator = (int) Math.round(decimal);
        int greatestCommonFactor = greatestCommonFactor(numerator, denominator);
        numerator1 = numerator / greatestCommonFactor;
        demonintor1 = denominator / greatestCommonFactor;
    }

    public int greatestCommonFactor(int num, int denom) {
        if (denom == 0) {
            return num;
        }
        return greatestCommonFactor(denom, num % denom);
    }


    private void convertDecimaltoAll(String number) {
        if (number.length() > 0)
        {
            decimalD = Double.valueOf(number);
            Fraction(Double.parseDouble(String.format("%.2f", decimalD)));
            res = numerator1 - 1 * demonintor1;
            fractionResult = (res) + "/" + demonintor1;
            edFractional.setText("" + fractionResult);
            Double d1 = decimalD;
            Double temp = (1 / d1) * 100;
            txtProbality.setText(String.format("%.2f", temp) + "%");

            if (decimalD >= 2) {
                Double valPostive = (decimalD - 1) * 100;
                i = Integer.valueOf(valPostive.intValue());
                edMoneyLine.setText("+" + "" + i);
            }

            else if (numerator1 - 1 == 0) {
                edMoneyLine.setText("NAN");
            }
            else
                {
                Double valNegative = (-100) / (decimalD - 1);
                j = valNegative.intValue();
                edMoneyLine.setText("" + j);
            }
        }
    }
    private void getAllValue() {
        moneyLineD = Double.parseDouble(edMoneyLine.getText().toString().trim());
        decimalD = Double.parseDouble(edDecimal.getText().toString().trim());
        fractionalD = Double.parseDouble(edFractional.getText().toString().trim());
    }
    private void convertMoneyLineToall(String number) {
        moneyLineD = Double.parseDouble(edMoneyLine.getText().toString().trim());
        if (number.length() > 0) {
            if (number.startsWith("-")) {
                String a = number.replace("-", "");
                Double valD = ((100 / Double.parseDouble(a)) + 1);
                edDecimal.setText(String.format("%.2f", valD));   //-odd to decimal

                Fraction(Double.parseDouble(String.format("%.2f", valD)));
                res = numerator1 - 1 * demonintor1;
                fractionResult = (res) + "/" + demonintor1;
                edFractional.setText("" + fractionResult);

                Double d1 = Double.parseDouble(number);   //+odd probality
                Double temp = ((-1) * (d1));
                Double temp1 = ((-1) * (d1) + 100);
                Double result = (temp / temp1) * 100;
                txtProbality.setText(String.format("%.2f", result) + "%");
            }
            else
                {
                Double val = (Double.parseDouble(number) / 100) + 1;
                edDecimal.setText(String.valueOf(val));     //+odd to decimal
                Double val1 = (Double.parseDouble(number) / 100);
                Fraction(val1);
                String fResult = (numerator1) + "/" + demonintor1;
                edFractional.setText("" + fResult);    //+oddd to fractionl

                Double d1 = Double.parseDouble(number);   //+odd probality
                Double temp = ((100) / (d1 + 100) * 100);
                DecimalFormat form = new DecimalFormat("0.00");
                txtProbality.setText(form.format(temp) + "%");
            }
        }
    }
    private void fractionToAll(String number) {
        if (number.contains("/")) {
            String[] rat = number.split("/");
            String n = rat[0];
            String v = rat[1];
            Double valD = Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v)) + 1);
            edDecimal.setText(String.format("%.2f", valD));

            Double temp = Double.parseDouble(v) / (Double.parseDouble(n) + Double.parseDouble(v));
            Double dd = 100 * temp;
            txtProbality.setText(String.format("%.2f", dd) + "%");

            if (Double.valueOf(Double.parseDouble(n)) > Double.valueOf(Double.parseDouble(v))) {
                Double valM = (Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v)) * 100));
                Double a = Double.parseDouble(String.valueOf(valM));
                int i = a.intValue();
                edMoneyLine.setText("+" + "" + i);

            } else if (Double.valueOf(Double.parseDouble(n)) < Double.valueOf(Double.parseDouble(v))) {
                Double valm = ((100) / Double.valueOf((Double.parseDouble(n) / Double.parseDouble(v))));
                Double aa = Double.parseDouble(String.valueOf(valm));
                int j = aa.intValue();
                edMoneyLine.setText("-" + "" + j);
            }
        } }
        private void hideKeyboard(Activity context) {
        View v = context.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }
}



