package com.example.bettype.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.example.bettype.R;
import com.example.bettype.activities.MainActivity1;
import com.example.bettype.utilies.Global;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;


public class Othercalculator extends Fragment {
    Context sContext;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_othercalculator, container, false);
        sContext=getActivity();
        ButterKnife.bind(this,view);
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(sContext, MainActivity1.class));
            }
        });
        return view;
    }

    @OnClick(R.id.kelly_layout)
    public void kelly_layout()
    {
        Global.changeDashFragment(sContext,new KellyCriterion());
    }

    @OnClick(R.id.dutching_layout)
    public void dutching_layout()
    {
        Global.changeDashFragment(sContext,new Dutchingcalculator());
    }

    @OnClick(R.id.odds_layout)
    public void odds_layout()
    { Global.changeDashFragment(sContext,new OddsConverter());
    }
    @OnClick(R.id.matched_layout)
    public void matched_layout()
    {
        Global.changeDashFragment(sContext,new MatchedBetCalculator());
    }

    @OnClick(R.id.arbitrage_layout)
    public void arbitrage_layout()
    {
        Global.changeDashFragment(sContext,new ArbitrageCalculator()); }}




