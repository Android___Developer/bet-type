package com.example.bettype.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.activities.SettingActivtity;
import com.example.bettype.adapters.ArbitageAdapter;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;

public class ArbitrageCalculator extends Fragment {
    Context sContext;
    @BindView(R.id.reyclerview)
    RecyclerView reyclerview;
    ArbitageAdapter adapter;
    @BindView(R.id.spinner_bettype)
    Spinner spinner_bettype;
    String[] arb = {"First Bet Amount", "Target Profit"};
    String[] commision = {"No", "Yes"};
    List<String> list = new ArrayList<>();
    @BindView(R.id.txt_count)
    TextView txt_count;
    AlertDialog alertDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_arbitrage_calculator, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        list.add("1");
        list.add("2");
        recycler();

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        return view;
    }

    private void recycler() {
        reyclerview.setLayoutManager(new LinearLayoutManager(sContext));
        adapter = new ArbitageAdapter(sContext, list);
        reyclerview.setAdapter(adapter);
    }

    @OnClick(R.id.txt_plus)
    public void txt_plus() {
        if (list.size() > 1 && list.size() < 15) {
            int i = list.size() + 1;
            list.add(i + "");
            txt_count.setText("" + i);
            adapter.notifyItemInserted(i);
            scrollToBottom();
        } else {
        }
    }

    private void scrollToBottom() {
        reyclerview.scrollToPosition(list.size() - 1);
    }


    @OnClick(R.id.txt_minus)
    public void txt_minus() {
        Log.e("size", list.size() + "");
        if (list.size() > 2) {
            list.remove(list.size() - 1);
            adapter.notifyItemRemoved(list.size());
            txt_count.setText("" + list.size());
            scrollToBottom();
        } else {
            Toast.makeText(sContext, "can't delet", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.img_setting)
    public void img_setting() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(sContext);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.alert_arbcalculator, null);
        dialogBuilder.setView(dialogView);
        Spinner spinner_commision = dialogView.findViewById(R.id.spinner_commision);
        Spinner spinner_arb_method = dialogView.findViewById(R.id.spinner_arb_method);

        TextView txt_setting = dialogView.findViewById(R.id.txt_setting);
        alertDialog = dialogBuilder.create();
        alertDialog.show();
        Global.spinner(sContext, R.layout.spinner_item, commision, spinner_commision);
        Global.spinner(sContext, R.layout.spinner_item, arb, spinner_arb_method);

        txt_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(sContext, "save", Toast.LENGTH_SHORT).show();
                alertDialog.dismiss();
            }
        });
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.TOP;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

    }

}
