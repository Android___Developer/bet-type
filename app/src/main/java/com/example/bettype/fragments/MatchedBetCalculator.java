package com.example.bettype.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.utilies.Helper;

import static com.example.bettype.activities.MainActivity1.backbutton;
import static com.example.bettype.activities.MainActivity1.search;
import static com.example.bettype.activities.MainActivity1.searchright;
import static com.example.bettype.activities.MainActivity1.setting;

public class MatchedBetCalculator extends Fragment {
    Context sContext;
    private Spinner spinner_bettype;
    String text;
    Double edBetAmountD, edBackOddD, edLayOddD, edBackCommissonD, edLayCommissionD;
    private String[] type = {"Qualifier", "Bonus Bet (SNR)", "Bonus Bet (SR)"};
    private TextView txtOverAllProfitLoss, txtLayLiabilty, txtLayAmount;
    private TextView txtBackBookmarker, txtBackExchange, txtBackProfitLoss,
            txtLayBookmarker, txtLayExchange, txtLayProfitLoss;
    private EditText edBetAmount, edBackOdd, edBackCommisson, edLayOdd, edLayCommission;
    private LinearLayout linearBackgroundClk;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matched_bet_calculator, container, false);
        sContext = getActivity();
        backbutton.setVisibility(View.VISIBLE);
        search.setVisibility(View.GONE);
        searchright.setVisibility(View.VISIBLE);
        setting.setVisibility(View.GONE);
        init(view);
        stakeSpinner();

        keyboradDoneListener();
        clickEvent();
        return view;
    }

    private void clickEvent() {
        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        linearBackgroundClk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Helper.hideKeyboard((Activity) sContext);
                    commonCheck();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private void stakeSpinner() {
        ArrayAdapter heatAdapter = new ArrayAdapter(sContext, R.layout.spinner_item, R.id.text, type);
        spinner_bettype.setAdapter(heatAdapter);
        spinner_bettype.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                text = parent.getSelectedItem().toString();
                try {
                    if (text.equals("Qualifier")) {
                        qualifieroverAllProfitLoss();
                    } else if (text.equals("Bonus Bet (SNR)")) {
                        betSnrConversion();
                    } else if (text.equals("Bonus Bet (SR)")) {
                        betSimpleSnrConversion();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void init(View view) {
        txtBackBookmarker = view.findViewById(R.id.txtBackBookmarker);
        txtBackExchange = view.findViewById(R.id.txtBackExchange);
        txtBackProfitLoss = view.findViewById(R.id.txtBackProfitLoss);
        txtLayBookmarker = view.findViewById(R.id.txtLayBookmarker);
        txtLayExchange = view.findViewById(R.id.txtLayExchange);
        txtLayProfitLoss = view.findViewById(R.id.txtLayProfitLoss);
        txtOverAllProfitLoss = view.findViewById(R.id.txtOverAllProfitLoss);
        spinner_bettype = view.findViewById(R.id.spinner_bettype);
        edBetAmount = view.findViewById(R.id.edBetAmount);
        edBackOdd = view.findViewById(R.id.edBackOdd);
        edBackCommisson = view.findViewById(R.id.edBackCommisson);
        edLayOdd = view.findViewById(R.id.edLayOdd);
        edLayCommission = view.findViewById(R.id.edLayCommission);
        txtLayAmount = view.findViewById(R.id.txtLayAmount);
        txtLayLiabilty = view.findViewById(R.id.txtLayLiabilty);
        linearBackgroundClk = view.findViewById(R.id.linearBackgroundClk);
    }

    private void qualifieroverAllProfitLoss() {
        getAllValue();
        //LAY AMOUNT for even PROFIT / LOSS
        Double a = edBackOddD - 1;
        Double b = ((edBackCommissonD / 100) * a);
        Double c = edBackOddD - b;
        Double d = edBetAmountD * c;
        Double f = edLayOddD - (edLayCommissionD / 100);
        Double result = d / f;
        txtLayAmount.setText("$" + "" + String.format("%.2f", result));
        Double val = (result) * (edLayOddD - 1);
        txtLayLiabilty.setText("$" + "" + String.format("%.2f", val));
        Double v = (val * (-1));
        txtBackExchange.setText("$" + "" + String.format("%.2f", v));

        //BOOKMAKER: back bet win
        Double aa = edBackOddD - 1;
        Double bb = aa * (edBackCommissonD / 100);
        Double cc = (edBackOddD - bb) - 1;
        Double result1 = cc * edBetAmountD;
        txtBackBookmarker.setText("$" + "" + String.format("%.2f", result1));

        //IF 'BACK' BET WINS
        Double aaa1 = result1 + v;
        txtBackProfitLoss.setText("$" + "" + String.format("%.2f", aaa1));
        txtOverAllProfitLoss.setText("$" + "" + String.format("%.2f", aaa1));

        //IF 'LAY' BET WINS:
        Double layExchange = result * (1 - (edLayCommissionD / 100));
        txtLayExchange.setText("$" + "" + String.format("%.2f", layExchange));
        txtLayBookmarker.setText("$" + "" + "-" + edBetAmountD);
        Double layResult = layExchange - edBetAmountD;
        txtLayProfitLoss.setText("$" + "" + String.format("%.2f", layResult));
    }

    private void betSnrConversion() {
        getAllValue();
        //LAY AMOUNT for even PROFIT / LOSS:
        Double a = edBackOddD - 1;
        Double b = (edBackCommissonD / 100) * a;
        Double c = edBackOddD - b;
        Double d = (c - 1) * edBetAmountD;
        Double e = edLayOddD - (edLayCommissionD / 100);
        Double result = d / e;
        txtLayAmount.setText("$" + "" + String.format("%.2f", result));

        //LAY LIABILITY:
        Double aLib = result * (edLayOddD - 1);
        txtLayLiabilty.setText("$" + "" + String.format("%.2f", aLib));

        //IF 'BACK' BET WINS: bookie , exchange , ProfitLoss
        Double a1 = edBackOddD - 1;
        Double b1 = (edBackCommissonD / 100) * a1;
        Double c1 = edBackOddD - b1;
        Double d1 = c1 - 1;
        Double resultBooke = d1 * edBetAmountD;

        txtBackBookmarker.setText("$" + "" + String.format("%.2f", resultBooke));
        Double aXchange = aLib * (-1);
        txtBackExchange.setText("$" + "" + String.format("%.2f", aXchange));
        Double backProiftLoss = resultBooke + aXchange;
        txtBackProfitLoss.setText("$" + "" + String.format("%.2f", backProiftLoss));
        txtOverAllProfitLoss.setText("$" + "" + String.format("%.2f", backProiftLoss));

        //IF 'BACK' LAY WINS: bookie , exchange , ProfitLoss
        txtLayBookmarker.setText("$" + " " + "0");
        Double aExchageLay = (result * (1 - edLayCommissionD / 100));
        txtLayExchange.setText("$" + "" + String.format("%.2f", aExchageLay));
        Double layProfitLoss = 0 + aExchageLay;
        txtLayProfitLoss.setText("$" + "" + String.format("%.2f", layProfitLoss));
    }

    private void getAllValue() {
        edBetAmountD = Double.parseDouble(edBetAmount.getText().toString().trim());
        edBackOddD = Double.parseDouble(edBackOdd.getText().toString().trim());
        edLayOddD = Double.parseDouble(edLayOdd.getText().toString().trim());
        String val = edBackCommisson.getText().toString().trim();

        if (val.equals("")) {
            edBackCommissonD = Double.parseDouble("0");
        } else {
            edBackCommissonD = Double.parseDouble(val); }

        String val1 = edLayCommission.getText().toString().trim();
        if (val1.equals("")) {
            edLayCommissionD = Double.parseDouble("0");
        } else {
            edLayCommissionD = Double.parseDouble(val1);
        }
    }

    private void betSimpleSnrConversion() {
        getAllValue();
        //LAY AMOUNT for even PROFIT / LOSS
        Double a = edBackOddD - 1;
        Double b = ((edBackCommissonD / 100) * a);
        Double c = edBackOddD - b;
        Double d = edBetAmountD * c;
        Double f = edLayOddD - (edLayCommissionD / 100);
        Double result = d / f;
        txtLayAmount.setText("$" + "" + String.format("%.2f", result));
        //LAY LIABILITY:
        Double aLib = result * (edLayOddD - 1);
        txtLayLiabilty.setText("$" + "" + String.format("%.2f", aLib));
        //IF 'BACK' BET WINS: bookie , exchange , ProfitLoss
        Double aa = edBackOddD - 1;
        Double bb = (edBackCommissonD / 100) * aa;
        Double cc = edBackOddD - bb;
        Double dd = (cc - 1) * edBetAmountD;
        Double res = dd + edBetAmountD;
        txtBackBookmarker.setText("$" + "" + String.format("%.2f", res));

        Double aExchageBack = (-1) * aLib;
        txtBackExchange.setText("$" + "" + String.format("%.2f", aExchageBack));
        Double backProiftLoss = res + aExchageBack;
        txtBackProfitLoss.setText("$" + "" + String.format("%.2f", backProiftLoss));
        txtOverAllProfitLoss.setText("$" + "" + String.format("%.2f", backProiftLoss));

        //IF 'BACK' LAY WINS: bookie , exchange , ProfitLoss
        txtLayBookmarker.setText("$" + " " + "0");
        Double aExchageLay = (result * (1 - (edLayCommissionD / 100)));
        txtLayExchange.setText("$" + "" + String.format("%.2f", aExchageLay));
        Double layProfitLoss = 0 + aExchageLay;
        txtLayProfitLoss.setText("$" + "" + String.format("%.2f", layProfitLoss));
    }

    private void keyboradDoneListener() {
        edBackOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String number = edBackOdd.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            edBackOddD = Double.parseDouble(number);
                            commonCheck();
                        } catch (Exception e) {
                        } }
                    return true; }
                return false;
            }
        });

        edBackCommisson.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String number = edBackCommisson.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            edBackCommissonD = Double.parseDouble(number);
                            commonCheck();
                        } catch (Exception e) {
                        }
                    }
                    return true; }
                return false;
            }
        });

        edLayCommission.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String number = edLayCommission.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            commonCheck();
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });

        edLayOdd.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String number = edLayOdd.getText().toString().trim();
                    if (number.length() > 0) {
                        try {
                            hideKeyboard((Activity) sContext);
                            edLayOddD = Double.parseDouble(number);
                            commonCheck();
                        } catch (Exception e) {
                        }
                    }
                    return true;
                }
                return false;
            }
        });
    }

    private void hideKeyboard(Activity context) {
        View v = context.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    private void commonCheck() {
        if (text.equals("Qualifier")) {
            qualifieroverAllProfitLoss();
        } else if (text.equals("Bonus Bet (SNR)")) {
            betSnrConversion();
        } else if (text.equals("Bonus Bet (SR)")) {
            betSimpleSnrConversion();
        }
    }
}
