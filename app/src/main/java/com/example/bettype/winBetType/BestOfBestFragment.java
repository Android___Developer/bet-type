package com.example.bettype.winBetType;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.adapters.TopFlucAdapter;
import com.example.bettype.model.FlucModel;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BestOfBestFragment extends Fragment {
    Context sContext;
    @BindView(R.id.reycler_best)
    RecyclerView reycler;
    @BindView(R.id.lin)
    LinearLayout lin;
    TopFlucAdapter adapter;
    List<FlucModel> list = new ArrayList<>();
    @BindView(R.id.txt_reptuble)
    TextView txt_reptuble;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bestofbest, container, false);
        sContext = getActivity();
        ButterKnife.bind(this, view);
        //  setColorSpan1(getResources().getString(R.string.best_two),7,31,txt_reptuble);
        list.add(new FlucModel(R.drawable.ic_betstar));
        list.add(new FlucModel(R.drawable.ic_palmer));
        list.add(new FlucModel(R.drawable.ic_ladbroke));
        list.add(new FlucModel(R.drawable.ic_lackjointoday));
        list.add(new FlucModel(R.drawable.ic_neds));
        list.add(new FlucModel(R.drawable.ic_topbetaa));
        reycler();
        return view;
    }

    private void reycler() {
        reycler.setLayoutManager(new GridLayoutManager(sContext, 2));
        adapter = new TopFlucAdapter((getActivity()), list);
        reycler.setAdapter(adapter);
        reycler.setNestedScrollingEnabled(false);

        adapter.onItemClickListner(new TopFlucAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.urlLink(sContext, "https://www.betstar.com.au/?signup=1&token=azILpuATnUg-XzFTxdIa1GNd7ZgqdRLk&b=&d=&a=1071&c=2&gclid=");
                } else if (pos == 1) {
                    Global.urlLink(sContext, "https://www.palmerbet.com/createaccount?aff=BDqxf9Xz0M5WXtV1TmvVNGNd7ZgqdRLk");
                } else if (pos == 2) {
                    Global.urlLink(sContext, "http://www.ladbrokes.com.au/lp/0005/?bonus=DM250&a=510766&c=2&d=FBD250");

                } else if (pos == 3) {
                    Global.urlLink(sContext, "https://www.bookmaker.com.au/?signup=1&token=0FN-D27EMOfnLzNd2EWh4WNd7ZgqdRLk&b=&d=&a=1027&c=2&gclid=");

                } else if (pos == 4) {
                    Global.urlLink(sContext, "https://www.neds.com.au/");
                } else if (pos == 5) {
                    Global.urlLink(sContext, "https://www.topbetta.com.au/registration/step-1?btag=uq6-6yuloNAIRyA9EA3AQ2Nd7ZgqdRLk&");
                } else {

                } }
        });

    }

//    private void setColorSpan() {
//        String text = getResources().getString(R.string.best_two);
//        SpannableString ss = new SpannableString(text);
//        ClickableSpan cs = new ClickableSpan() {
//            @Override
//            public void onClick(View widget) {
//                Toast.makeText(sContext, "sadd", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void updateDrawState(TextPaint ds) {
//                super.updateDrawState(ds);
//                ds.setUnderlineText(false);
//                ds.setColor(getResources().getColor(R.color.colorPrimary));
//            }
//        };
//        ss.setSpan(cs, 8, 31, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
//        ss.setSpan(new RelativeSizeSpan(1.1f), 8, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        ss.setSpan(new android.text.style.StyleSpan(Typeface.NORMAL), 8, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//        txt_reptuble.setText(ss);
//        txt_reptuble.setMovementMethod(LinkMovementMethod.getInstance());
//    }


    private void setColorSpan1(String text, int start, int end, TextView textView) {
        String txt = text;
        //    getResources().getString(R.string.best_two);
        SpannableString ss = new SpannableString(txt);
        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Toast.makeText(sContext, " you clicked here", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.colorPrimary));
            }
        };
        ss.setSpan(cs, start, end, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new RelativeSizeSpan(1.1f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new android.text.style.StyleSpan(Typeface.NORMAL), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
    }


}