package com.example.bettype.winBetType;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bettype.R;
import com.example.bettype.adapters.TopFlucAdapter;
import com.example.bettype.model.FlucModel;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ParlayWinBetFragment extends Fragment {
    Context sContext;
    List<FlucModel> list=new ArrayList<>();
    TopFlucAdapter adapter;
    @BindView(R.id.reycler)
    RecyclerView reycler;
    @BindView(R.id.txt_one)
    TextView txt_one;
    @BindView(R.id.txt_two)
    TextView txt_two;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parlay_winbet, container, false);
        sContext = getActivity();
        ButterKnife.bind(this,view);
        setColorSpan1(getResources().getString(R.string.formula_combo),0,22,txt_one);
        setColorSpan1(getResources().getString(R.string.formula_combo_two),0,22,txt_two);
        list.add(new FlucModel(R.drawable.ic_jointoday));
        reycler();
        return view;
    }

    private void reycler() {
        reycler.setLayoutManager(new GridLayoutManager(sContext,2));
        adapter=new TopFlucAdapter((getActivity()),list);
        reycler.setAdapter(adapter);
        reycler.setNestedScrollingEnabled(false);
        adapter.onItemClickListner(new TopFlucAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.urlLink(sContext, "http://www.tab.com.au/");
                } else {

                } }
        });

    }

    private void setColorSpan1(String text, int start, int end, TextView textView) {
        String  txt =text;
        //    getResources().getString(R.string.best_two);
        SpannableString ss = new SpannableString(txt);
        ClickableSpan cs = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.light_black));
            }
        };
        ss.setSpan(cs, start, end, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new RelativeSizeSpan(1.1f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new android.text.style.StyleSpan(Typeface.BOLD), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        textView.setText(ss);
        textView.setMovementMethod(LinkMovementMethod.getInstance());

    }
}
