package com.example.bettype.winBetType;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.bettype.R;
import com.example.bettype.adapters.TopFlucAdapter;
import com.example.bettype.model.FlucModel;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ToteTopPlusFragment extends Fragment {
    Context sContext;
    List<FlucModel> list=new ArrayList<>();
    @BindView(R.id.reycler_tote_plus)
    RecyclerView reycler_tote_plus;
    TopFlucAdapter adapter;
    @BindView(R.id.lin)
    LinearLayout lin;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tote_top, container, false);
        sContext=getActivity();
        ButterKnife.bind(this,view);
        list.add(new FlucModel(R.drawable.ic_beteasyy));
        list.add(new FlucModel(R.drawable.ic_sportsbett));
        reycler();
        return view; }

    private void reycler() {
        reycler_tote_plus.setLayoutManager(new GridLayoutManager(sContext,2));
        adapter=new TopFlucAdapter((getActivity()),list);
        reycler_tote_plus.setAdapter(adapter);
        reycler_tote_plus.setNestedScrollingEnabled(false);

        adapter.onItemClickListner(new TopFlucAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.urlLink(sContext, "https://beteasy.com.au/join-now?");
                } else if (pos == 1) {
                    Global.urlLink(sContext, "https://promotions.sportsbet.com.au/racing-501-adss/?dclid=CjkKEQjwp5_qBRCStI748dqJuZoBEiQAOMNeBLktUfTSOxCr_0svqbxH0gH8KioDFUaumWfCYEfPuiLw_wcB");

                }  else {

                } }
        });


    }
}
