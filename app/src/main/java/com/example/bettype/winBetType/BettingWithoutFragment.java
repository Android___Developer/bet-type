package com.example.bettype.winBetType;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bettype.R;
import com.example.bettype.adapters.TopFlucAdapter;
import com.example.bettype.model.FlucModel;
import com.example.bettype.utilies.Global;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BettingWithoutFragment extends Fragment {
    @BindView(R.id.reycler_place_without)
    RecyclerView reycler_place_without;
    List<FlucModel> list=new ArrayList<>();
    TopFlucAdapter adapter;
    Context sContext;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_betting_without, container, false);
        sContext = getActivity();
        ButterKnife.bind(this,view);
        list.add(new FlucModel(R.drawable.ic_sportsbett));
        reycler();
        return view;
    }

    private void reycler() {
        reycler_place_without.setLayoutManager(new GridLayoutManager(sContext,2));
        adapter=new TopFlucAdapter((getActivity()),list);
        reycler_place_without.setAdapter(adapter);
        reycler_place_without.setNestedScrollingEnabled(false);
        adapter.onItemClickListner(new TopFlucAdapter.OnItemClickInterface() {
            @Override
            public void onItemClick(int pos) {
                if (pos == 0) {
                    Global.urlLink(sContext, "https://promotions.sportsbet.com.au/racing-501-adss/?dclid=CjkKEQjwp5_qBRCStI748dqJuZoBEiQAOMNeBJIuq_51hTiJ2SM64H_ZpLRuw4IXl1oprRJR-veP0yTw_wcB");
                } else {

                } }
        });


    }
}
